'use strict';
angular.module('core.home', [
	'ngMaterial',
    'ui.router',
    'angularMoment',
    'ngLodash',
]);