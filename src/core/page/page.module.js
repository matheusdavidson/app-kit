(function() {
    'use strict';
    angular.module('core.page', [
        'core.menu',
        'anim-in-out',
        'ui.utils.masks',
        'directives.inputMatch'
    ]);
})();