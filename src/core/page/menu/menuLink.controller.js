(function() {
    'use strict';
    angular.module('core.menu').controller('MenuLinkCtrl', /*@ngInject*/ function($scope, $state, $location, $mdSidenav, $anchorScroll, $timeout) {
        var vm = this;
        vm.state = $state;
        vm.goAndClose = function(route, state, anchor, event, section) {
            if (!state || !anchor) defaultAction(route);

            if (state && anchor) scrollToAction(state, anchor, event, section);
        }

        function defaultAction(route) {
            //
            // Transition to path
            // 
            $location.path(route);

            //
            // Close sidenav
            // 
            $mdSidenav('left').close();
        }

        function scrollToAction(state, anchor, event, section) {
            var currentState = $state.current.name;

            // 
            // Override state to go if anchorSelf is set
            // 
            state = section.anchorSelf ? currentState : state;
            
            if (state == currentState) {
                event.stopPropagation();
                var off = $scope.$on('$locationChangeStart', function(ev) {
                    off();
                    ev.preventDefault();
                });

                scroll(anchor);
            } else {
                $state.go(state);
                $scope.$on('$stateChangeSuccess', function(ev) {

                    $timeout(function() {
                        event.stopPropagation();
                        var off = $scope.$on('$locationChangeStart', function(ev) {
                            off();
                            ev.preventDefault();
                        });

                        scroll(anchor);
                    }, 2000);
                });
            }
        }

        function scroll(anchor) {
            //
            // Close sidenav
            // 
            $mdSidenav('left').close();

            $timeout(function() {
                $location.hash(anchor);
                $anchorScroll();
            }, 500);
        }
    })
})();