(function() {
    angular.module("app.setting", []).constant("setting", {
        name: "app kit",
        slug: "app-kit",
        version: "0.0.1",
        title: "app kit",
        baseUrl: "",
        titleSeparator: " — ",
        description: "app-kit",
        copyright: "app-kit",
        google: {
            clientId: "",
            language: "pt-BR"
        },
        facebook: {
            scope: "email",
            appId: "",
            appSecret: "",
            language: "pt-BR"
        },
        https: [],
        redirWww: false,
        ogLocale: "pt_BR",
        ogSiteName: "app-kit",
        ogTitle: "app-kit",
        ogDescription: "app-kit",
        ogUrl: "",
        ogImage: "",
        ogSection: "app-kit",
        ogTag: "app-kit"
    });
})();