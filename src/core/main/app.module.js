(function() {
    'use strict';
    angular.module('core.app', [
        'app.setting',
        'app.env',
        'core.utils',
        'core.page',
        'core.login',
        'core.user',
        // 'core.profile',
        'core.account',
        'core.home'
    ]);
})();