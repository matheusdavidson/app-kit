(function() {
    'use strict';
    angular.module('app.kit', [
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'satellizer',
        'angulartics',
        'angulartics.google.analytics',
        'ui.router',
        'ngMaterial',
        'toastr',
        'app.setting',
        'app.env',
        'core.app'
    ]);
})();