(function() {
    'use strict';
    angular.module('core.account', [
        'core.utils',
        'core.user',
        'core.menu',
        'ui.router',
        'vAccordion',
        'angularMoment',
        'ngLodash',
        'ngMask',
        'angularMoment',
        'satellizer'
    ])
})();