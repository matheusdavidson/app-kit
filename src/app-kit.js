(function() {
    'use strict';
    angular.module('app.kit', [
        'ngAnimate',
        'ngCookies',
        'ngTouch',
        'ngSanitize',
        'ngMessages',
        'ngAria',
        'satellizer',
        'angulartics',
        'angulartics.google.analytics',
        'ui.router',
        'ngMaterial',
        'toastr',
        'app.setting',
        'app.env',
        'core.app'
    ]);
})();
(function() {
    'use strict';
    angular.module('core.account', [
        'core.utils',
        'core.user',
        'core.menu',
        'ui.router',
        'vAccordion',
        'angularMoment',
        'ngLodash',
        'ngMask',
        'angularMoment',
        'satellizer'
    ])
})();
(function() {
    'use strict';
    angular.module('core.home', ['ui.router']);
})();
(function() {
    'use strict';
    /**
     * @ngdoc overview
     * @name core.login
     * @requires app.env
     * @requires app.setting
     * @requires satellizer
     **/
    angular.module('core.login', [
        'app.env',
        'app.setting',
        'ui.router',
        'satellizer',
        'google.login',
        'facebook.login'
    ]);
})();
(function() {
    'use strict';
    angular.module('core.app', [
        'app.setting',
        'app.env',
        'core.utils',
        'core.page',
        'core.login',
        'core.user',
        // 'core.profile',
        'core.account',
        'core.home'
    ]);
})();
(function() {
    'use strict';
    angular.module('core.page', [
        'core.menu',
        'anim-in-out',
        'ui.utils.masks',
        'directives.inputMatch'
    ]);
})();
(function() {
    'use strict';
    angular.module('core.user', ['satellizer']);
})();
(function() {
    'use strict';
    angular.module('core.utils', ['ImageCropper']);
})();
(function() {
    'use strict';
    angular.module('facebook.login', [
        'facebook',
        'app.env',
        'app.setting'
    ]);
})();
(function() {
    'use strict';
    angular.module('google.login', [
        'app.env',
        'app.setting',
        'directive.g+signin'
    ])
})();
(function() {
    'use strict';
    angular.module('core.menu', ['ui.router', 'truncate']);
})();
(function() {
    'use strict';
    angular.module('core.account').provider('$account',
        /**
         * @ngdoc object
         * @name core.account.$accountProvider
         * @description
         * 2 em 1 - provém configurações e a factory (ver $get) com estados/comportamentos de conta.
         **/
        /*@ngInject*/
        function $accountProvider() {
            /**
             * @ngdoc object
             * @name core.account.$accountProvider#_instance
             * @propertyOf core.account.$accountProvider
             * @description
             * Instância de conta armazenada pelo {@link core.account.service:$Account serviço}
             **/
            this._instance = {};
            /**
             * @ngdoc object
             * @name core.account.$accountProvider#_config
             * @propertyOf core.account.$accountProvider
             * @description
             * armazena configurações
             **/
            this._config = {};
            /**
             * @ngdoc object
             * @name core.account.$accountProvider#_templateUrl
             * @propertyOf core.account.$accountProvider
             * @description
             * url do template para a rota
             **/
            this._templateUrl = 'core/account/account.tpl.html';
            /**
             * @ngdoc object
             * @name core.account.$accountProvider#_confirmTemplateUrl
             * @propertyOf core.account.$accountProvider
             * @description
             * url do template para confirmação de conta
             **/
            this._confirmTemplateUrl = 'core/account/confirm.tpl.html';
            /**
             * @ngdoc function
             * @name core.account.$accountProvider#$get
             * @propertyOf core.account.$accountProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($account) {
             *      console.log($account.templateUrl);
             *      //prints the current templateUrl
             *      //ex.: "core/account/account.tpl.html"
             *      console.log($account.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto correspondente a uma Factory
             **/
            this.$get = this.get = function() {
                return {
                    config: this._config,
                    templateUrl: this._templateUrl,
                    confirmTemplateUrl: this._confirmTemplateUrl,
                    /**
                     * @ngdoc function
                     * @name core.account.$accountProvider#set
                     * @methodOf core.account.$accountProvider
                     * @description
                     * Setar instância da conta
                     * @example
                     * <pre>
                     * var account = new $Account();
                     * $account.set(account);
                     * //now account instance can be injectable
                     * angular.module('myApp').controller('myCtrl',function($account){
                     * console.log($account.instance) //imprime objeto de instância da conta
                     * })
                     * </pre>
                     **/
                    set: function(data) {
                        this._instance = data;
                        return data;
                    },
                    instance: function() {
                        return this._instance
                    },
                    /**
                     * @ngdoc function
                     * @name core.account.$accountProvider#destroy
                     * @methodOf core.account.$accountProvider
                     * @description
                     * Apagar instância da conta
                     * @example
                     * <pre>
                     * var account = new $Account();
                     * $account.set(account);
                     * //now account instance can be injectable
                     * angular.module('myApp').controller('myCtrl',function($account){
                     * $account.instance.destroy() //apaga instância da conta
                     * })
                     * </pre>
                     **/
                    destroy: function() {
                        this.instance = {};
                    }
                }
            }
            /**
             * @ngdoc function
             * @name core.account.$accountProvider#config
             * @methodOf core.account.$accountProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *     $accountProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }
            /**
             * @ngdoc function
             * @name core.account.$accountProvider#templateUrl
             * @methodOf core.account.$accountProvider
             * @description
             * getter/setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.templateUrl('app/account/my-account.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.templateUrl = function(val) {
                if (val) return this._templateUrl = val;
                else return this._templateUrl;
            }
            /**
             * @ngdoc function
             * @name core.account.$accountProvider#confirmTemplateUrl
             * @methodOf core.account.$accountProvider
             * @description
             * getter/setter para url do template de confirmação de conta
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($accountProvider) {
             *      $accountProvider.confirmTemplateUrl('app/account/my-account-confirm.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.confirmTemplateUrl = function(val) {
                if (val) return this._confirmTemplateUrl = val;
                else return this._confirmTemplateUrl;
            }
        });
})();
(function() {
    'use strict';
    /*global window*/
    angular.module('core.home').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider) {
        /**
         * States & Routes
         */
        $stateProvider.state('app.home', {
            url: '/',
            views: {
                'content': {
                    templateUrl: 'core/home/home.tpl.html',
                    controller: '$HomeCtrl as vm'
                }
            },
            resolve: {
                closeMenu: /*@ngInject*/ function($timeout, $auth, $menu) {
                    if ($auth.isAuthenticated()) {
                        $timeout(function() {
                            $menu.api().close();
                        }, 500)
                    }
                }
            }
        });
        $locationProvider.html5Mode(true);
    })
})();
(function() {
    'use strict';
    angular.module('core.home').controller('$HomeCtrl', /*@ngInject*/ function($rootScope, $page, setting) {
        var vm = this;
        //
        // SEO
        //
        $page.title(setting.name + setting.titleSeparator + ' Home');

        //
        // Broadcast
        //
        $rootScope.$on('$LoginSuccess', function(ev, response) {
            console.log(response)
        });

        bootstrap();

        function bootstrap() {}
    });
})();
(function() {
    'use strict';
    angular.module('core.login').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider, $loginProvider) {
        //
        // States & Routes
        //
        $stateProvider.state('app.login', {
            protected: false,
            url: '/login/',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $loginProvider.templateUrl()
                    },
                    controller: '$LoginCtrl as vm'
                }
            },
            resolve: {
                authed: /*@ngInject*/ function($auth, $location, $login) {
                    if ($auth.isAuthenticated()) {
                        $location.path($login.config.auth.loginSuccessRedirect);
                    }
                }
            }
        }).state('app.logout', {
            protected: false,
            url: '/logout/',
            views: {
                'content': {
                    controller: '$LogoutCtrl as vm'
                }
            }
        }).state('app.signup', {
            protected: false,
            url: '/signup/',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $loginProvider.signupTemplateUrl()
                    },
                    controller: /*@ngInject*/ function($page, setting) {
                        $page.title(setting.name + setting.titleSeparator + 'Cadastro');
                    }
                },
                resolve: {
                    authed: /*@ngInject*/ function($auth, $location, $login) {
                        if ($auth.isAuthenticated()) {
                            $location.path($login.config.auth.loginSuccessRedirect);
                        }
                    }
                }
            }
        }).state('app.login-lost', {
            protected: false,
            url: '/login/lost/',
            views: {
                'content': {
                    templateUrl: /*@ngInject*/ function() {
                        return $loginProvider.lostTemplateUrl()
                    },
                    controller: '$LostCtrl as vm'
                }
            },
            resolve: {
                authed: /*@ngInject*/ function($auth, $window, $login) {
                    if ($auth.isAuthenticated()) {
                        $window.location = $login.config.auth.loginSuccessRedirect //here we use $window to fix issue related with $location.hash (#) in url
                    }
                }
            }
        });
        $locationProvider.html5Mode(true);
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc object
     * @name core.login.controller:$LoginCtrl
     * @requires core.login.$loginProvider
     * @requires core.page.factory:$page
     * @requires setting
     * @requires api
     **/
    'use strict';
    angular.module('core.login').controller('$LoginCtrl', /*@ngInject*/ function($rootScope, $scope, $state, $auth, $http, $mdToast, $location, $login, $page, setting, api) {
        $page.title(setting.name + setting.titleSeparator + 'Login');
        $page.description('Entre para o ' + setting.name);
        $page.load.done();
        var vm = this;
        vm.config = $login.config;
    })
})();
'use strict';
/**
 * @ngdoc directive
 * @name core.login.directive:login
 * @restrict EA
 * @description 
 * Diretiva "wrapper" pro template de login
 * @element div
 **/
angular.module('core.login').directive('login', /*@ngInject*/ function() {
    return {
        scope: {},
        templateUrl: 'core/login/login.tpl.html',
        controller: '$LoginCtrl',
        controllerAs: 'vm',
        restrict: 'EA'
    }
});
(function() {
    'use strict';
    angular.module('core.login').provider('$login',
        /**
         * @ngdoc object
         * @name core.login.$loginProvider
         **/
        /*@ngInject*/
        function $loginProvider() {
            /**
             * @ngdoc object
             * @name core.login.$loginProvider#_config
             * @propertyOf core.login.$loginProvider
             * @description
             * armazena configurações
             **/
            this._config = {};
            /**
             * @ngdoc object
             * @name core.login.$loginProvider#_templateUrl
             * @propertyOf core.login.$loginProvider
             * @description
             * url do template para a rota
             **/
            this._templateUrl = 'core/login/login.tpl.html';
            /**
             * @ngdoc object
             * @name core.login.$loginProvider#_signupTemplateUrl
             * @propertyOf core.login.$loginProvider
             * @description
             * url do template para novos cadastros
             **/
            this._signupTemplateUrl = 'core/login/register/register.tpl.html';
            /**
             * @ngdoc object
             * @name core.login.$loginProvider#_lostTemplateUrl
             * @propertyOf core.login.$loginProvider
             * @description
             * url do template para recuperação de senha
             **/
            this._lostTemplateUrl = 'core/login/register/lost.tpl.html';
            /**
             * @ngdoc function
             * @name core.login.$loginProvider#$get
             * @propertyOf core.login.$loginProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($login) {
             *      console.log($login.templateUrl);
             *      //prints the current templateUrl of `core.login`
             *      //ex.: "core/login/login.tpl.html"
             *      console.log($login.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades. ex: config e templateUrl
             **/
            this.$get = this.get = function() {
                return {
                    config: this._config,
                    templateUrl: this._templateUrl,
                    signupTemplateUrl: this._signupTemplateUrl
                }
            }
            /**
             * @ngdoc function
             * @name core.login.$loginProvider#config
             * @methodOf core.login.$loginProvider
             * @description
             * setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($loginProvider) {
             *     $loginProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             *     $loginProvider.config('signupWelcome','Olá @firstName, você entrou para a @appName');
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }
            /**
             * @ngdoc function
             * @name core.login.$loginProvider#templateUrl
             * @methodOf core.login.$loginProvider
             * @description
             * setter para url do template
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($loginProvider) {
             *      $loginProvider.templateUrl('app/login/my-login.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.templateUrl = function(val) {
                if (val) return this._templateUrl = val;
                else return this._templateUrl;
            }
            /**
             * @ngdoc function
             * @name core.login.$loginProvider#lostTemplateUrl
             * @methodOf core.login.$loginProvider
             * @description
             * setter para url do template para recuperação de senha
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($loginProvider) {
             *      $loginProvider.lostTemplateUrl('app/login/my-login-lost.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.lostTemplateUrl = function(val) {
                if (val) return this._lostTemplateUrl = val;
                else return this._lostTemplateUrl;
            }
            /**
             * @ngdoc function
             * @name core.login.$loginProvider#signupTemplateUrl
             * @methodOf core.login.$loginProvider
             * @description
             * setter para url do template de novos cadastros
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($loginProvider) {
             *      $loginProvider.signupTemplateUrl('app/login/my-signup.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.signupTemplateUrl = function(val) {
                if (val) return this._signupTemplateUrl = val;
                else return this._signupTemplateUrl;
            }
        });
})();
(function() {
    'use strict';
    /**
     * @ngdoc object
     * @name core.login.controller:$LogoutCtrl
     * @description
     * Destruir sessão
     * @requires core.login.$user
     **/
    angular.module('core.login').controller('$LogoutCtrl', /*@ngInject*/ function($user) {
        var userInstance = $user.instance();
        if (typeof userInstance.destroy === 'function') $user.instance().destroy();
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc object
     * @name core.login.controller:$LostCtrl
     * @requires core.page.factory:$page
     * @requires setting
     * @requires api
     **/
    angular.module('core.login').controller('$LostCtrl', /*@ngInject*/ function($state, $auth, $http, $mdToast, $location, $page, setting, api) {
        $page.title(setting.name + setting.titleSeparator + 'Mudar senha');
        $page.description('Entre para o ' + setting.name);
        $page.load.done();
        var vm = this;
        vm.lost = lost;
        vm.change = change;
        //lost password step2
        var userHash = $location.hash();
        if (userHash) vm.userHash = userHash;
        /**
         * @ngdoc function
         * @name core.login.controller:$LostCtrl#change
         * @methodOf core.login.controller:$LostCtrl
         * @description
         * Alterar senha
         * @param {string} pw senha
         **/
        function change(pw) {
            $page.load.init();
            var onSuccess = function(data) {
                $page.load.done();
                $state.transitionTo('app.login');
                $mdToast.show($mdToast.simple().content(data.success).position('bottom right').hideDelay(3000))
            }
            var onError = function(data) {
                $page.load.done();
                $mdToast.show($mdToast.simple().content(data.error).position('bottom right').hideDelay(3000))
            }
            $http.put(api.url + "/api/users/" + userHash + '/newPassword', {
                password: pw
            }).success(onSuccess).error(onError);
        }
        /**
         * @ngdoc function
         * @name core.login.controller:$LostCtrl#lost
         * @methodOf core.login.controller:$LostCtrl
         * @description
         * Link para alteração de senha
         * @param {string} email email
         **/
        function lost(email) {
            $page.load.init();
            var onSuccess = function(data) {
                $page.load.done();
                $mdToast.show($mdToast.simple().content(data.success).position('bottom right').hideDelay(3000))
            }
            var onError = function(data) {
                $page.load.done();
                $mdToast.show($mdToast.simple().content(data.error).position('bottom right').hideDelay(3000))
            }
            $http.post(api.url + "/api/users/lost", {
                email: email
            }).success(onSuccess).error(onError);
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.app').config( /*@ngInject*/ function($appProvider, $logProvider, $urlMatcherFactoryProvider, $stateProvider, $urlRouterProvider, $locationProvider, $mdThemingProvider, $authProvider, $httpProvider, $loginProvider, $userProvider, enviroment, setting, api) {
        //
        // States & Routes
        //    
        $stateProvider.state('app', {
            abstract: true,
            views: {
                'app': {
                    templateUrl: /*@ngInject*/ function() {
                        return $appProvider.layoutUrl();
                    },
                },
                'toolbar@app': {
                    templateUrl: /*@ngInject*/ function() {
                        return $appProvider.toolbarUrl();
                    }
                },
                'sidenav@app': {
                    templateUrl: /*@ngInject*/ function() {
                        return $appProvider.sidenavUrl();
                    }
                }
            }
        });
        $locationProvider.html5Mode(true);
        //
        // Redirect Trailing Slash
        //
        $urlMatcherFactoryProvider.strictMode(false);
        $urlRouterProvider.rule(function($injector, $location) {
            if ($location.hash() !== 'iframe') {
                var path = $location.url();
                // check to see if the path already has a slash where it should be
                if (path[path.length - 1] === '/' || path.indexOf('/?') > -1) {
                    return;
                }
                if (path.indexOf('?') > -1) {
                    return path.replace('?', '/?');
                }
                return path + '/';
            }
        });
        //
        // Intercept Http
        //
        $httpProvider.interceptors.push('HttpInterceptor');
        //
        // Theme options
        //
        $mdThemingProvider.theme('default').primaryPalette('indigo', {
            // 'hue-1': '600'
        }).accentPalette('deep-orange', {
            // 'hue-1': '600'
        });
        //
        // Dark theme
        //
        $mdThemingProvider.theme('darkness').primaryPalette('cyan').dark();
        //
        // Auth options
        //
        $authProvider.httpInterceptor = true; // Add Authorization header to HTTP request
        $authProvider.loginOnSignup = true;
        $authProvider.loginRedirect = '/profile/';
        $authProvider.logoutRedirect = '/login/';
        $authProvider.signupRedirect = '/login/';
        $authProvider.loginUrl = api.url + '/auth/local/';
        $authProvider.signupUrl = api.url + '/api/users/';
        $authProvider.loginRoute = '/login/';
        $authProvider.signupRoute = '/login/';
        $authProvider.tokenRoot = false; // set the token parent element if the token is not the JSON root
        $authProvider.tokenName = 'token';
        $authProvider.tokenPrefix = setting.slug + '.session'; // Local Storage name prefix
        $authProvider.unlinkUrl = '/auth/unlink/';
        $authProvider.unlinkMethod = 'get';
        $authProvider.authHeader = 'Authorization';
        $authProvider.withCredentials = true; // Send POST request with credentials
        //
        // Module Configs
        //
        $loginProvider.config('auth', {
            loginFailStateRedirect: 'app.login',
            loginSuccessStateRedirect: 'app.profile',
            loginSuccessRedirect: '/profile/'
        });
        $userProvider.setting('logoutStateRedirect', 'app.home');
        $userProvider.setting('roleForCompany', 'profile');
        //
        // Debug handle
        //
        if (enviroment === 'production')
            $logProvider.debugEnabled(false);
        else
            $logProvider.debugEnabled(true);
    });
})();
(function() {
    'use strict';
    /* global moment */
    /**
     * @ngdoc object
     * @name app.kit.controller:$AppCtrl
     * @description
     * Controlador da aplicação
     * @requires setting
     * @requires environment
     * @requires $rootScope
     * @requires $scope
     * @requires $state
     * @requires $location
     * @requires $mdSidenav
     * @requires $timeout
     * @requires $auth
     * @requires core.page.factory:$page
     * @requires core.user.service:$User
     * @requires core.user.factory:$user
     * @requires core.login.$loginProvider
     * @requires core.page.factory:$menu
     **/
    angular.module('core.app').controller('$AppCtrl', /*@ngInject*/ function(setting, $rootScope, $scope, $state, $location, $mdSidenav, $timeout, $auth, $page, $User, $user, enviroment, $menu, $login, $app) {
        var vm = this;
        vm.enviroment = enviroment;
        //
        // SEO
        //
        $page.title(setting.title);
        $page.description(setting.description);
        //
        // OPEN GRAPH
        //
        $page.ogLocale(setting.ogLocale);
        $page.ogSiteName(setting.ogSiteName);
        $page.ogTitle(setting.ogTitle);
        $page.ogDescription(setting.ogDescription);
        $page.ogUrl(setting.ogUrl);
        $page.ogImage(setting.ogImage);
        $page.ogSection(setting.ogSection);
        $page.ogTag(setting.ogTag);
        //
        // Moment
        //
        moment.locale('pt_BR');
        //
        // Events
        //  
        $rootScope.$on('$AppReboot', function() {
            bootstrap();
        });
        $rootScope.$on('$CompanyIdUpdated', function(e, nv, ov) {
            if (nv != ov) {
                //quando alterar company, atualizar factory  
                var company = $user.instance().filterCompany(nv);
                $user.instance().current('company', company);
                $user.instance().session('company', {
                    _id: company._id,
                    name: company.name
                });
                $menu.api().close();
                bootstrap();
            }
        });
        $rootScope.$on('$Unauthorized', function(ev, status) {
            //
            // Persistir o local atual
            // para redirecionamento após o login
            // - somente se status 401
            //
            if (status === 401) {
                $app.storage('session').set({
                    locationRedirect: $location.url()
                });
            }
            $rootScope.$Unauthorized = true;
            var userInstance = $user.instance();
            if (typeof userInstance.destroy === 'function') $user.instance().destroy();
        });
        //
        // Comportamentos para quando o usuário entrar
        //
        $rootScope.$on('$LoginSuccess', function(ev, response) {
            //
            // Redirecionar usuario para alguma rota pre-estabelecida
            //
            var appSession = $app.storage('session').get();
            if (appSession && appSession.locationRedirect && appSession.locationRedirect != '/login/') {
                //
                // Redirecionar o caboclo
                //
                $location.path(appSession.locationRedirect);
                //
                // Resetar o locationRedirect
                //
                $app.storage('session').set({
                    locationRedirect: ''
                })
            } else {
                $location.path($login.config.auth.loginSuccessRedirect);
            }
            
            //
            // Zerar o $rootScope.$Unauthorized
            //
            $rootScope.$Unauthorized = false;
        });
        //
        // BOOTSTRAP
        //  
        bootstrap(true);

        function bootstrap(withUser) {
            if (withUser) {
                var newUser = new $User();
                $user.set(newUser);
            }

            vm.user = $user.instance();
            vm.$page = $page;
            vm.setting = setting;
            vm.year = moment().format('YYYY');
            vm.state = $state;
            vm.isAuthed = $auth.isAuthenticated;
            vm.logout = logout;
            vm.menu = $menu.api();
            vm.loginConfig = $login.config;
            vm.iframe = $location.hash() === 'iframe' ? true : false;
            vm.logo = $app.logo;
            vm.logoWhite = $app.logoWhite;
        }
        //
        // Behaviors
        //
        function logout() {
            $mdSidenav('left').close();
            $timeout(function() {
                var userInstance = $user.instance();
                if (typeof userInstance.destroy === 'function') $user.instance().destroy(true);
                bootstrap(true);

                $state.go('app.login');
            }, 500);
        }
        //
        // Redirect http to https //@bug - bug com _escaped_fragment_ - redirecionando via CF
        // https://github.com/esgrupo/livejob/issues/15
        //
        // function http2https() {
        //     //tenho https configurado
        //     if (setting.https.length) {
        //         //host esta na configuração
        //         if (_.indexOf(setting.https, $location.host().replace(/www./g, '')) != -1) {
        //             //protocolo atual não é seguro            
        //             if ($location.protocol() !== 'https') {
        //                 //estou na home #15 bug com _escaped_fragment_
        //                 if ($state.current.name === 'app.home') {
        //                     //bingo
        //                     $window.location.href = $location.absUrl().replace(/http/g, 'https');
        //                 }
        //             }
        //         }
        //     }
        // }
        //
        // Redirect non-www to www
        // https://github.com/esgrupo/livejob/issues/17
        //
        function nonWww2www() {
            //redirecionar www
            if (setting.redirWww) {
                //se estiver em produção
                if (enviroment === 'production') {
                    if (!hasWww()) {
                        $window.location.href = 'https://www.' + $location.host() + $location.path();
                    }
                }
            }
        }

        function hasWww() {
            var www = new RegExp("www.");
            return www.test($location.host());
        }
    })
})();
(function() {
    'use strict';
    angular.module("app.env", []).constant("enviroment", "development").constant("api", {
        url: "http://localhost:9000"
    });
})();
(function() {
    'use strict';
    angular.module('core.app').provider('$app',
        /**
         * @ngdoc object
         * @name app.kit.$appProvider
         * @description
         * Provém configurações para aplicação
         **/
        /*@ngInject*/
        function $appProvider($stateProvider) {
            /**
             * @ngdoc object
             * @name app.kit.$appProvider#_config
             * @propertyOf app.kit.$appProvider
             * @description
             * armazena configurações
             **/
            this._config = {};

            /**
             * @ngdoc object
             * @name app.kit.$appProvider#_layoutUrl
             * @propertyOf app.kit.$appProvider
             * @description
             * url do template para layout
             **/
            this._layoutUrl = 'core/page/layout/layout.tpl.html';

            /**
             * @ngdoc object
             * @name app.kit.$appProvider#_toolbarUrl
             * @propertyOf app.kit.$appProvider
             * @description
             * url do template para toolbar
             **/
            this._toolbarUrl = 'core/page/toolbar/toolbar.tpl.html';

            /**
             * @ngdoc object
             * @name app.kit.$appProvider#_toolbarTitleUrl
             * @propertyOf app.kit.$appProvider
             * @description
             * url do template para o toolbar title
             **/
            this._toolbarTitleUrl = 'core/page/toolbar/title/toolbarTitle.tpl.html';

            /**
             * @ngdoc object
             * @name app.kit.$appProvider#_sidenavUrl
             * @propertyOf app.kit.$appProvider
             * @description
             * url do template para sidenav
             **/
            this._sidenavUrl = 'core/page/menu/sidenav.tpl.html';

            /**
             * @ngdoc object
             * @name app.kit.$appProvider#_logo
             * @propertyOf app.kit.$appProvider
             * @description
             * armazena logo
             **/
            this._logo = '';

            /**
             * @ngdoc object
             * @name app.kit.$appProvider#_logoWhite
             * @propertyOf app.kit.$appProvider
             * @description
             * armazena logo na versão branca
             **/
            this._logoWhite = '';

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#$get
             * @propertyOf app.kit.$appProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($app) {
             *      console.log($app.layoutUrl);
             *      //prints the default layoutUrl
             *      //ex.: "core/page/layout/layout.tpl.html"
             *      console.log($app.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($window, setting) {
                return {
                    config: this._config,
                    layoutUrl: this._layoutUrl,
                    toolbarUrl: this._toolbarUrl,
                    toolbarTitleUrl: this._toolbarTitleUrl,
                    sidenavUrl: this._sidenavUrl,
                    logoWhite: this._logoWhite,
                    logo: this._logo,
                    /**
                     * @ngdoc method
                     * @name app.kit.$appProvider#storage
                     * @methodOf app.kit.$appProvider
                     * @description
                     * Carregar/persistir dados
                     * @param {string} type tipo da persistência (local/session)
                     * @return {object} getter/setter para persistência de dados
                     **/
                    storage: function(type) {
                        var where = (type === 'local') ? 'localStorage' : 'sessionStorage';
                        return {
                            set: function(item) {
                                return $window[where].setItem(setting.slug + '.app', angular.toJson(item));
                            },
                            get: function() {
                                return angular.fromJson($window[where].getItem(setting.slug + '.app'));
                            },
                            destroy: function() {
                                $window[where].removeItem(setting.slug + '.app');
                            }
                        }
                    }
                }
            }

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#config
             * @methodOf app.kit.$appProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($appProvider) {
             *     $appProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (val) return this._config[key] = val;
                else return this._config[key];
            }

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#logo
             * @methodOf app.kit.$appProvider
             * @description
             * getter/setter para o path da logo
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($appProvider) {
             *     $appProvider.logo('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logo = function(value) {
                if (value) return this._logo = value;
                else return this._logo;
            }

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#logoWhite
             * @methodOf app.kit.$appProvider
             * @description
             * getter/setter para o path da logo na versão branca
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($appProvider) {
             *     $appProvider.logoWhite('assets/images/my-logo.png')
             * })
             * </pre>
             * @param {string} value caminho para logomarca
             **/
            this.logoWhite = function(value) {
                if (value) return this._logoWhite = value;
                else return this._logoWhite;
            }

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#layoutUrl
             * @methodOf app.kit.$appProvider
             * @description
             * getter/setter para url do layout
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($appProvider) {
             *      $appProvider.layoutUrl('app/layout/my-layout.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.layoutUrl = function(val) {
                if (val) return this._layoutUrl = val;
                else return this._layoutUrl;
            }

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#toolbarUrl
             * @methodOf app.kit.$appProvider
             * @description
             * getter/setter para url do toolbar
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($appProvider) {
             *      $appProvider.toolbarUrl('app/layout/my-toolbar.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.toolbarUrl = function(val) {
                if (val) return this._toolbarUrl = val;
                else return this._toolbarUrl;
            }

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#toolbarTitleUrl
             * @methodOf app.kit.$appProvider
             * @description
             * getter/setter para url do componente toolbar-title
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($appProvider) {
             *      $appProvider.toolbarUrl('app/layout/my-toolbar.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.toolbarTitleUrl = function(val) {
                if (val) return this._toolbarTitleUrl = val;
                else return this._toolbarTitleUrl;
            }

            /**
             * @ngdoc function
             * @name app.kit.$appProvider#sidenavUrl
             * @methodOf app.kit.$appProvider
             * @description
             * getter/setter para url do sidenav
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($appProvider) {
             *      $appProvider.sidenavUrl('app/layout/my-sidenav.html')
             * })
             * </pre>
             * @param {string} val url do template
             **/
            this.sidenavUrl = function(val) {
                if (val) return this._sidenavUrl = val;
                else return this._sidenavUrl;
            };
        }
    )
})();
(function() {
    angular.module("app.setting", []).constant("setting", {
        name: "app kit",
        slug: "app-kit",
        version: "0.0.1",
        title: "app kit",
        baseUrl: "",
        titleSeparator: " — ",
        description: "app-kit",
        copyright: "app-kit",
        google: {
            clientId: "",
            language: "pt-BR"
        },
        facebook: {
            scope: "email",
            appId: "",
            appSecret: "",
            language: "pt-BR"
        },
        https: [],
        redirWww: false,
        ogLocale: "pt_BR",
        ogSiteName: "app-kit",
        ogTitle: "app-kit",
        ogDescription: "app-kit",
        ogUrl: "",
        ogImage: "",
        ogSection: "app-kit",
        ogTag: "app-kit"
    });
})();
(function() {
    'use strict';
    /*global window*/
    angular.module('core.page').config( /*@ngInject*/ function($stateProvider, $urlRouterProvider, $locationProvider) {
        /**
         * States & Routes (@todo - é preciso dar replace nesta config pelas apps filhas)
         */
        // $stateProvider.state('app.pages', {
        //     protected: false,
        //     url: '/p/:slug/',
        //     views: {
        //         'content': {
        //             templateUrl: 'core/page/page.tpl.html',
        //             controller: '$PageCtrl as vm'
        //         }
        //     },
        //     resolve: {
        //         slug: /*@ngInject*/ function($stateParams) {
        //             return $stateParams.slug;
        //         },
        //         closeMenu: /*@ngInject*/ function($timeout, $auth, $menu) {
        //             if ($auth.isAuthenticated()) {
        //                 $timeout(function() {
        //                     $menu.api().close();
        //                 }, 500)
        //             }
        //         }
        //     }
        // });
        // $locationProvider.html5Mode(true);
    })
})();
(function() {
    'use strict';
    angular.module('core.page').controller('$PageCtrl', /*@ngInject*/ function($page, setting) {
        var vm = this;
        //
        // SEO
        //
        $page.title(setting.name + setting.titleSeparator + ' Home');
        bootstrap();

        function bootstrap() {}
    });
})();
(function() {
    'use strict';
    angular.module('core.page').provider('$page',
        /**
         * @ngdoc object
         * @name core.page.$pageProvider
         * @description
         * Provém configurações/comportamentos/estados para página
         **/
        /*@ngInject*/
        function $pageProvider() {
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_config
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena configurações
             **/
            this._config = {
                // configuração para ativar/desativar a rota inicial
                'homeEnabled': true
            };

            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_title
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena o título
             **/
            this._title = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_description
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena a descrição
             **/
            this._description = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogSiteName
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph site name
             **/
            this._ogSiteName = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogTitle
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph title
             **/
            this._ogTitle = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogDescription
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph description
             **/
            this._ogDescription = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogUrl
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph url
             **/
            this._ogUrl = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogImage
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph image
             **/
            this._ogImage = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogSection
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph section
             **/
            this._ogSection = '';
            /**
             * @ngdoc object
             * @name core.page.$pageProvider#_ogTag
             * @propertyOf core.page.$pageProvider
             * @description
             * armazena open graph tags
             **/
            this._ogTag = '';
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#$get
             * @propertyOf core.page.$pageProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($page) {
             *      console.log($page.config('myOwnConfiguration'));
             *      //prints the current config
             *      //ex.: "{ configA: 54, configB: '=D' }"
             * })
             * </pre>
             * @return {object} Retorna um objeto contendo valores das propriedades.
             **/
            this.$get = this.get = /*@ngInject*/ function($mdToast) {
                return {
                    config: this._config,
                    load: load(),
                    progress: progress(),
                    toast: toast($mdToast),
                    title: title,
                    description: description,
                    ogLocale: ogLocale,
                    ogSiteName: ogSiteName,
                    ogTitle: ogTitle,
                    ogDescription: ogDescription,
                    ogUrl: ogUrl,
                    ogImage: ogImage,
                    ogSection: ogSection,
                    ogTag: ogTag
                }
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#config
             * @methodOf core.page.$pageProvider
             * @description
             * getter/setter para configurações
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($pageProvider) {
             *     $pageProvider.config('myOwnConfiguration', {
             *          configA: 54,
             *          configB: '=D'
             *      })
             * })
             * </pre>
             * @param {string} key chave
             * @param {*} val valor
             **/
            this.config = function(key, val) {
                if (key && (val || val === false)) {
                    return this._config[key] = val
                } else if (key) {
                    return this._config[key]
                } else {
                    return this._config
                }
            }

            /**
             * @ngdoc function
             * @name core.page.$pageProvider#title
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para meta tag título
             * @param {string} str título da página
             * @return {string} título da página
             **/
            function title(value) {
                if (value) return this._title = value;
                else return this._title;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#description
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para meta tag descrição
             * @param {string} value descrição da página
             **/
            function description(value) {
                if (value) return this._description = value;
                else return this._description;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogLocale
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph locale
             * @param {string} value locale
             **/
            function ogLocale(value) {
                if (value) return this._ogLocale = value;
                else return this._ogLocale;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogSiteName
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph site name
             * @param {string} value site name
             **/
            function ogSiteName(value) {
                if (value) return this._ogSiteName = value;
                else return this._ogSiteName;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogTitle
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph title
             * @param {string} value title
             **/
            function ogTitle(value) {
                if (value) return this._ogTitle = value;
                else return this._ogTitle;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogDescription
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph description
             * @param {string} value description
             **/
            function ogDescription(value) {
                if (value) return this._ogDescription = value;
                else return this._ogDescription;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogUrl
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph url
             * @param {string} value url
             **/
            function ogUrl(value) {
                if (value) return this._ogUrl = value;
                else return this._ogUrl;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogImage
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph image
             * @param {string} value image
             **/
            function ogImage(value) {
                if (value) return this._ogImage = value;
                else return this._ogImage;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogSection
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph section
             * @param {string} value section
             **/
            function ogSection(value) {
                if (value) return this._ogSection = value;
                else return this._ogSection;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#ogTag
             * @methodOf core.page.$pageProvider
             * @description
             * getter/getter para open-graph tag
             * @param {string} value tag
             **/
            function ogTag(value) {
                if (value) return this._ogTag = value;
                else return this._ogTag;
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#load
             * @methodOf core.page.$pageProvider
             * @description
             * inicia e termina o carregamento da página
             * @return {object} com metodos de inicialização (init) e finalização (done)
             **/
            function load() {
                return {
                    init: function() {
                        this.status = true;
                        //console.log('loader iniciado...' + this.status);
                    },
                    done: function() {
                        this.status = false;
                        //console.log('loader finalizado...' + this.status);
                    }
                }
            }
            /**
             * @ngdoc function
             * @name core.page.$pageProvider#toast
             * @methodOf core.page.$pageProvider
             * @description
             * mostra uma mensagem de aviso
             * @param {string} msg mensagem
             * @param {integer} time tempo em milisegundos
             * @param {string} position posição do alerta. default: 'bottom right'
             **/
            function toast($mdToast) {
                return function(msg, time, position) {
                    time = time ? time : 5000;
                    $mdToast.show($mdToast.simple().content(msg).position(position ? position : 'bottom right').hideDelay(time));
                }
            }
            //another type of load
            function progress() {
                return {
                    init: function() {
                        this.status = true;
                        //console.log('progress iniciado...' + this.status);
                    },
                    done: function() {
                        this.status = false;
                        //console.log('progress finalizado...' + this.status);
                    }
                }
            }
        }
    )
})();
(function() {
    'use strict';
    angular.module('core.user').provider('$user',
        /**
         * @ngdoc object
         * @name core.user.$userProvider
         * @description
         * 2 em 1 - provém configurações e a factory (ver $get) com estados/comportamentos de usuário.
         **/
        /*@ngInject*/
        function $userProvider() {
            /**
             * @ngdoc object
             * @name core.user.$userProvider#_instance
             * @propertyOf core.user.$userProvider
             * @description
             * Instância de usuário armazenada pelo {@link core.user.service:$User serviço}
             **/
            this._instance = null;
            /**
             * @ngdoc object
             * @name core.user.$userProvider#_setting
             * @propertyOf core.user.$userProvider
             * @description
             * Armazena configurações
             **/
            this._setting = {};
            /**
             * @ngdoc function
             * @name core.user.$userProvider#$get
             * @propertyOf core.user.$userProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($user) {
             *      console.log($user.setting.roleForCompany);
             *      //printa a regra para empresa
             * })
             * </pre>
             * @return {object} objeto correspondente a uma Factory
             **/
            this.$get = this.get = function() {
                return {
                    instance: function() {
                        return this._instance;
                    },
                    setting: this._setting,
                    /**
                     * @ngdoc function
                     * @name core.user.$userProvider#set
                     * @methodOf core.user.$userProvider
                     * @description
                     * Setar instância do usuário
                     * @example
                     * <pre>
                     * var user = new $User();
                     * $user.set(user);
                     * //now user instance can be injectable
                     * angular.module('myApp').controller('myCtrl',function($user){
                     * console.log($user.instance()) //imprime objeto de instância do usuário
                     * })
                     * </pre>
                     **/
                    set: function(data) {
                        this._instance = data;
                        return data;
                    },
                    /**
                     * @ngdoc function
                     * @name core.user.$userProvider#destroy
                     * @methodOf core.user.$userProvider
                     * @description
                     * Apagar instância do usuário
                     * @example
                     * <pre>
                     * var user = new $User();
                     * $user.set(user);
                     * //now user instance can be injectable
                     * angular.module('myApp').controller('myCtrl',function($user){
                     * $user.instance().destroy() //apaga instância do usuário
                     * })
                     * </pre>
                     **/
                    destroy: function() {
                        this._instance = null;
                    }
                }
            }
            this.setting = function(key, val) {
                if (key && val) return this._setting[key] = val;
                else if (key) return this._setting[key];
                else return this._setting;
            }
        })
})();
(function() {
    'use strict';
    /* global window */
    angular.module('core.user').service('$User', /*@ngInject*/ function($rootScope, $state, $http, $auth, $timeout, $user, $menu, $page, $window, setting) {
        /**
         * @ngdoc service
         * @name core.user.service:$User
         * @description
         * Model de usuário
         * @param {object} params propriedades da instância
         * @param {bool} alert aviso de boas vindas
         * @param {string} message mensagem do aviso
         **/
        var User = function(params, alert, message) {
                /**
                 * @ngdoc object
                 * @name core.user.service:$User#params
                 * @propertyOf core.user.service:$User
                 * @description
                 * Propriedades da instância
                 **/
                params = params ? params : {};
                /**
                 * @ngdoc object
                 * @name core.user.service:$User#currentData
                 * @propertyOf core.user.service:$User
                 * @description
                 * Armazena dados customizados na instância do usuário
                 **/
                this.currentData = {};
                /**
                 * @ngdoc object
                 * @name core.user.service:$User#sessionData
                 * @propertyOf core.user.service:$User
                 * @description
                 * Armazena dados customizados no localStorage do usuário
                 **/
                this.sessionData = {};
                this.init(params, alert, message);
            }
            /**
             * @ngdoc function
             * @name core.user.service:$User:init
             * @methodOf core.user.service:$User
             * @description
             * Inicialização
             * @param {object} params propriedades da instância
             * @param {bool} alert aviso de boas vindas
             * @param {string} message mensagem do aviso
             */
        User.prototype.init = function(params, alert, message) {
            //set params
            if (typeof params === 'object') {
                angular.extend(this, params);
            }
            if (params._id) {
                var gender = (params.profile && params.profile.gender === 'F') ? 'a' : 'o',
                    roleForCompany = false;
                if ($user.setting.roleForCompany != 'user') roleForCompany = $user.setting.roleForCompany;
                if (roleForCompany && params[roleForCompany].role ? params[roleForCompany].role.length : params.role.length) {
                    this.current('company', getCompany(this));
                    this.current('companies', getCompanies(this));
                }
                if (!message) message = 'Olá ' + params.profile.firstName + ', você entrou. Bem vind' + gender + ' de volta.';
                if (alert) $page.toast(message, 10000);
                if (this.session('company') && this.session('company')._id) {
                    this.current('company', this.filterCompany(this.session('company')._id));
                }
                setStorageUser(params);
            } else {
                params = getStorageUser();
                if (params) return this.init(params);
            }
            return false;
        }
        /**
         * @ngdoc function
         * @name core.user.service:$User:current
         * @methodOf core.user.service:$User
         * @description
         * Adiciona informações customizadas no formato chave:valor à instância corrente do usuário
         * @example
         * <pre>
         * var user = new $User();
         * user.current('company',{_id: 123456, name: 'CocaCola'})
         * console.log(user.current('company')) //prints {_id: 123456, name: 'CocaCola'}
         * </pre>
         * @param {string} key chave
         * @param {*} val valor
         */
        User.prototype.current = function(key, val) {
            if (key && val) {
                if (!this.currentData) this.currentData = {};
                this.currentData[key] = val;
            } else if (key) {
                return this.currentData && this.currentData[key] ? this.currentData[key] : false;
            }
            return this.currentData;
        }
        /**
         * @ngdoc function
         * @name core.user.service:$User:session
         * @methodOf core.user.service:$User
         * @description
         * Adiciona informações customizadas no formato chave:valor à instância corrente do usuário e ao localStorage
         * @param {string} key chave
         * @param {*} val valor
         */
        User.prototype.session = function(key, val) {
            if (key && val) {
                if (!this.sessionData) this.sessionData = {};
                this.sessionData[key] = val;
                setStorageSession(this.sessionData);
            } else if (key) {
                this.sessionData = getStorageSession();
                return this.sessionData && this.sessionData[key] ? this.sessionData[key] : false;
            }
            this.sessionData = getStorageSession();
            return this.sessionData;
        }

        /**
         * @ngdoc function
         * @name core.user.service:$User:filterCompany
         * @methodOf core.user.service:$User
         * @description
         * Buscar uma empresa
         * @param {string} _id id da empresa
         * @return {object} objeto da empresa
         */
        User.prototype.filterCompany = function(_id) {
            var result = false,
                companies = getCompanies(this);
            if (companies && companies.length) {
                companies.forEach(function(row) {
                    if (row.company._id === _id) {
                        result = row.company;
                        return;
                    }
                });
            }
            return result;
        }

        /**
         * @ngdoc function
         * @name core.user.service:$User:destroy
         * @methodOf core.user.service:$User
         * @description
         * Destruir sessão do usuário
         * @param {bool} alert mensagem de aviso (você saiu)
         */
        User.prototype.destroy = function(alert) {
            $user.set({});
            removeStorageSession();
            removeStorageUser();
            $auth.removeToken();
            $auth.logout();
            $page.load.done();
            if (alert) $page.toast('Você saiu', 3000);
            $rootScope.$emit('$UserLeft');
        }
        /**
         * @ngdoc function
         * @name core.user.service:$User:getWorkPosition
         * @methodOf core.user.service:$User
         * @description
         * Obter a lista de cargos (@todo migrar para aplicações filhas)
         * @param {string} companyid id da empresa
         * @return {array} lista de cargos desejados
         */
        User.prototype.getWorkPosition = function(companyid) {
            var result = false,
                companies = getCompanies(this);
            if (companies.length) {
                companies.forEach(function(row) {
                    if (row.company._id === companyid) {
                        result = row.position;
                        return;
                    }
                });
            }
            return result;
        }
        User.prototype.profileUpdate = function(profile) {
            this.profile = profile;
            setStorageUser(this);
        }
        User.prototype.getCompany = getCompany;
        User.prototype.getCompanies = getCompanies;

        function token() {
            return $auth.getToken();
        }

        function getStorageUser() {
            return JSON.parse(window.localStorage.getItem(setting.slug + '.user'));
        }

        function setStorageUser(user) {
            return window.localStorage.setItem(setting.slug + '.user', JSON.stringify(user));
        }

        function removeStorageUser() {
            window.localStorage.removeItem(setting.slug + '.user');
            window.localStorage.removeItem(setting.slug + '.session_token');
        }

        function getStorageSession() {
            return JSON.parse(window.localStorage.getItem(setting.slug + '.session'));
        }

        function setStorageSession(session) {
            return window.localStorage.setItem(setting.slug + '.session', JSON.stringify(session));
        }

        function removeStorageSession() {
            window.localStorage.removeItem(setting.slug + '.session');
            window.localStorage.removeItem(setting.slug + '.app');
        }

        function getCompanies(userInstance) {
            var roleForCompany = false;
            if ($user.setting.roleForCompany != 'user') roleForCompany = $user.setting.roleForCompany;
            return roleForCompany && userInstance[roleForCompany] ? userInstance[roleForCompany].role : userInstance.role;
        }

        function getCompany(userInstance) {
            userInstance = userInstance ? userInstance : this;
            var roleForCompany = false;
            var userInstanceRoleCompany = userInstance && userInstance.role && userInstance.role.length && userInstance.role[0].company ? userInstance.role[0].company : false;
            if ($user.setting.roleForCompany != 'user') roleForCompany = $user.setting.roleForCompany;
            return roleForCompany ? userInstance[roleForCompany].role[0].company : userInstanceRoleCompany;
        }
        return User;
    })


})();
(function() {
    'use strict';
    /* jshint undef: false, unused: false, shadow:true, quotmark: false, -W110,-W117, eqeqeq: false */
    angular.module('core.utils').factory('$utils', /*@ngInject*/ function($q) {
        var vm = this;
        return {
            isImg: isImg,
            brStates: brStates,
            age: age
        }

        function isImg(src) {
            var deferred = $q.defer();
            var image = new Image();
            image.onerror = function() {
                deferred.resolve(false);
            };
            image.onload = function() {
                deferred.resolve(true);
            };
            image.src = src;
            return deferred.promise;
        }

        function age(date) {
            return moment(date).fromNow(true);
        }

        function brStates() {
            return [{
                value: "AC",
                name: "Acre"
            }, {
                value: "AL",
                name: "Alagoas"
            }, {
                value: "AM",
                name: "Amazonas"
            }, {
                value: "AP",
                name: "Amapá"
            }, {
                value: "BA",
                name: "Bahia"
            }, {
                value: "CE",
                name: "Ceará"
            }, {
                value: "DF",
                name: "Distrito Federal"
            }, {
                value: "ES",
                name: "Espírito Santo"
            }, {
                value: "GO",
                name: "Goiás"
            }, {
                value: "MA",
                name: "Maranhão"
            }, {
                value: "MT",
                name: "Mato Grosso"
            }, {
                value: "MS",
                name: "Mato Grosso do Sul"
            }, {
                value: "MG",
                name: "Minas Gerais"
            }, {
                value: "PA",
                name: "Pará"
            }, {
                value: "PB",
                name: "Paraíba"
            }, {
                value: "PR",
                name: "Paraná"
            }, {
                value: "PE",
                name: "Pernambuco"
            }, {
                value: "PI",
                name: "Piauí"
            }, {
                value: "RJ",
                name: "Rio de Janeiro"
            }, {
                value: "RN",
                name: "Rio Grande do Norte"
            }, {
                value: "RO",
                name: "Rondônia"
            }, {
                value: "RS",
                name: "Rio Grande do Sul"
            }, {
                value: "RR",
                name: "Roraima"
            }, {
                value: "SC",
                name: "Santa Catarina"
            }, {
                value: "SE",
                name: "Sergipe"
            }, {
                value: "SP",
                name: "São Paulo"
            }, {
                value: "TO",
                name: "Tocantins"
            }];
        }
    })
})();
(function() {
    'use strict';
    angular.module('facebook.login').config(function(FacebookProvider, setting) {
        FacebookProvider.init({
            version: 'v2.3',
            appId: setting.facebook.appId,
            locale: 'pt_BR'
        });
    });
})();
(function() {
    'use strict';
    angular.module('facebook.login').controller('FacebookLoginCtrl', /*@ngInject*/ function(fbLogin) {
        var vm = this;
        vm.login = login;

        function login() {
            fbLogin.go();
        }
    })
})();
(function() {
    'use strict';
    angular.module('facebook.login').directive('facebookLogin', /*@ngInject*/ function() {
        return {
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/login/facebook/facebookLogin.tpl.html";
            },
            scope: {
                user: '=',
                templateUrl: '='
            },
            controller: 'FacebookLoginCtrl',
            controllerAs: 'fb'
        }
    })
})();
(function() {
    'use strict';
    angular.module('facebook.login').factory('fbLogin', /*@ngInject*/ function($rootScope, $auth, $mdToast, $http, Facebook, $user, $page, $login, api, setting) {
        return {
            go: go
        }

        function go(cbSuccess, cbFail) {
            $page.load.init();
            Facebook.getLoginStatus(function(response) {
                if (response.status === 'connected') {
                    return loginHandler(cbSuccess, cbFail);
                } else {
                    Facebook.login(function(response) {
                        if (response.error || !response.status || !response.authResponse) {
                            $page.load.done();
                            return;
                        }
                        return loginHandler(cbSuccess, cbFail);
                    }, {
                        scope: setting.facebook.scope || 'email'
                    });
                }
            })
        }

        function me() {
            return Facebook.api('/me', function() {
                //$scope.user = response;
            });
        }

        function loginHandler(cbSuccess, cbFail) {
            var onSuccess = function(fbUser) {
                var onSuccess = function(response) {
                    $page.load.done();
                    var msg = false;
                    var gender = (response.data.user.profile && response.data.user.profile.gender && response.data.user.profile.gender === 'F') ? 'a' : 'o';
                    if (response.data.new) {
                        msg = 'Olá ' + response.data.user.profile.firstName + ', você entrou. Seja bem vind' + gender + ' ao ' + setting.name;
                        if ($login.config.signupWelcome) {
                            msg = $login.config.signupWelcome.replace('@firstName', response.data.user.profile.firstName).replace('@appName', setting.name);
                        }
                    }
                    $auth.setToken(response.data.token);
                    var userInstance = $user.instance();
                    if (typeof userInstance.init === 'function') $user.instance().init(response.data.user, true, msg);
                    if (cbSuccess) cbSuccess()
                    $rootScope.$emit('$LoginSuccess', response);
                }
                var onFail = function(response) {
                    $page.load.done();
                    $mdToast.show($mdToast.simple().content(response.data && result.data.error ? response.data.error : 'error').position('bottom right').hideDelay(3000))
                    if (cbFail) cbFail()
                }
                var gender = '';
                gender = fbUser.gender && fbUser.gender === 'female' ? 'F' : gender;
                $http.post(api.url + '/auth/facebook', {
                    provider: 'facebook',
                    id: fbUser.id,
                    firstName: fbUser.first_name,
                    lastName: fbUser.last_name,
                    email: fbUser.email,
                    gender: gender,
                    applicant: true
                }).then(onSuccess, onFail);
            }
            var onFail = function() {}
            me().then(onSuccess, onFail);
        }
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc object
     * @name core.login.controller:$LoginFormCtrl
     * @description
     * Controlador do componente
     * @requires $scope
     * @requires $auth
     * @requires $mdToast
     * @requires core.user.factory:$user
     **/
    angular.module('core.login').controller('$LoginFormCtrl', /*@ngInject*/ function($rootScope, $scope, $auth, $page, $mdToast, $user) {
        var vm = this;
        vm.login = login;
        /**
         * @ngdoc function
         * @name core.login.controller:$LoginFormCtrl#login
         * @propertyOf core.login.controller:$LoginFormCtrl
         * @description
         * Controlador do componente de login
         * @param {string} logon objeto contendo as credenciais email e password
         **/
        function login(logon) {
            $page.load.init();
            var onSuccess = function(response) {
                $page.load.done();
                var userInstance = $user.instance();
                if (typeof userInstance.init === 'function') $user.instance().init(response.data.user, true);
                $rootScope.$emit('$LoginSuccess', response);
            }
            var onError = function(result) {
                $page.load.done();
                $mdToast.show($mdToast.simple().content(result.data && result.data.error ? result.data.error : 'error').position('bottom right').hideDelay(3000))
            }
            $auth.login({
                email: logon.email,
                password: logon.password
            }).then(onSuccess, onError);
        }
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.login.directive:loginForm
     * @restrict EA
     * @description
     * Componente para o formulário de login
     * @element div
     * @param {object} config objeto de configurações do módulo login
     * @param {object} user objeto instância do usuário
     * @param {string} template-url caminho para o template do formulário
     **/
    angular.module('core.login').directive('loginForm', /*@ngInject*/ function() {
        return {
            scope: {
                config: '=',
                user: '=',
                templateUrl: '='
            },
            restrict: 'EA',
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/login/form/loginForm.tpl.html";
            },
            controller: '$LoginFormCtrl',
            controllerAs: 'vm',
            link: function() {}
        }
    });
})();
(function() {
    'use strict';
    /* global gapi */
    angular.module('google.login').controller('GoogleLoginCtrl', /*@ngInject*/ function($auth, $scope, $http, $mdToast, $state, $page, $user, setting, api) {
        var vm = this;
        vm.clientId = setting.google.clientId;
        vm.language = setting.google.language;
        $scope.$on('event:google-plus-signin-success', function( /*event, authResult*/ ) {
            // Send login to server or save into cookie
            gapi.client.load('plus', 'v1', apiClientLoaded);
        });
        $scope.$on('event:google-plus-signin-failure', function( /*event, authResult*/ ) {
            // @todo Auth failure or signout detected
        });

        function apiClientLoaded() {
            gapi.client.plus.people.get({
                userId: 'me'
            }).execute(handleResponse);
        }

        function handleResponse(glUser) {
            login(glUser);
        }

        function login(glUser) {
            $page.load.init();
            var onSuccess = function(response) {
                $page.load.done();
                var msg = false;
                var gender = (response.data.user.profile && response.data.user.profile.gender && response.data.user.profile.gender === 'F') ? 'a' : 'o';
                if (response.data.new) msg = 'Olá ' + response.data.user.profile.firstName + ', você entrou. Seja bem vind' + gender + ' ao ' + setting.name;
                $auth.setToken(response.data.token);
                var userInstance = $user.instance();
                if (typeof userInstance.init === 'function') $user.instance().init(response.data.user, true, msg);
            }
            var onFail = function(result) {
                $page.load.done();
                $mdToast.show($mdToast.simple().content(result.data && result.data.error ? result.data.error : 'error').position('bottom right').hideDelay(3000))
            }
            $http.post(api.url + '/auth/google', {
                provider: 'google',
                id: glUser.id,
                firstName: glUser.name.givenName,
                lastName: glUser.name.familyName,
                email: glUser.emails[0].value,
                gender: glUser.gender
            }).then(onSuccess, onFail);
        }
    })
})();
(function() {
    'use strict';
    angular.module('google.login').directive('googleLogin', /*@ngInject*/ function() {
        return {
            templateUrl: "core/login/google/googleLogin.tpl.html",
            controller: 'GoogleLoginCtrl',
            controllerAs: 'google'
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.login').controller('RegisterFormCtrl', /*@ngInject*/ function($scope, $auth, $mdToast, $user, $page, $login, setting) {
        $scope.register = register;
        $scope.sign = {};

        function register(sign) {
            $page.load.init();
            var onSuccess = function(result) {
                var msg = 'Olá ' + result.data.user.profile.firstName + ', você entrou para o ' + setting.name;
                if ($login.config.signupWelcome) {
                    msg = $login.config.signupWelcome.replace('@firstName', result.data.user.profile.firstName).replace('@appName', setting.name);
                }
                $page.load.done();
                var userInstance = $user.instance();
                if (typeof userInstance.init === 'function') $user.instance().init(result.data.user, true, msg, 10000);
            }
            var onError = function(result) {
                $page.load.done();
                $mdToast.show($mdToast.simple().content(result.data && result.data.error ? result.data.error : 'error').position('bottom right').hideDelay(10000))
            }
            $auth.signup({
                firstName: sign.firstName,
                lastName: sign.lastName,
                email: sign.email,
                password: sign.password,
                provider: 'local'
            }).then(onSuccess, onError);
        }
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.login.directive:registerForm
     * @restrict E
     * @description
     * Componente para o formulário de cadastro
     * @element div
     * @param {object} config objeto de configurações do módulo login
     * @param {string} template-url caminho para o template do formulário
     **/
    angular.module('core.login').directive('registerForm', /*@ngInject*/ function() {
        return {
            scope: {
                config: '=',
                templateUrl: '='
            },
            templateUrl: function(elem, attr) {
                return attr.templateUrl ? attr.templateUrl : "core/login/register/registerForm.tpl.html";
            },
            controller: 'RegisterFormCtrl',
            controlerAs: 'vm'
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.page').directive('loader', /*@ngInject*/ function() {
        return {
            templateUrl: "core/page/loader/loader.tpl.html",
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.menu').config( /*@ngInject*/ function() {})
})();
(function() {
    'use strict';
    angular.module('core.menu').provider('$menu',
        /**
         * @ngdoc object
         * @name core.menu.$menuProvider
         * @description
         * 2 em 1 - provém configurações e a factory (ver $get) com estados/comportamentos de menu.
         **/
        /*@ngInject*/
        function $menuProvider() {
            var instance = this;
            /**
             * @ngdoc object
             * @name core.menu.$menuProvider#mainMenu
             * @propertyOf core.menu.$menuProvider
             * @description
             * Armazena o menu principal
             **/
            this.mainMenu = [];
            /**
             * @ngdoc object
             * @name core.menu.$menuProvider#toolbarMenu
             * @propertyOf core.menu.$menuProvider
             * @description
             * Armazena o menu para a toolbar
             **/
            this.toolbarMenu = [];
            /**
             * @ngdoc function
             * @name core.menu.$menuProvider#$get
             * @propertyOf core.menu.$menuProvider
             * @description
             * getter que vira factory pelo angular para se tornar injetável em toda aplicação
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($menu) {
             *      console.log($menu.main); //printa array contendo itens do menu principal
             *      $menu.api().close() //fecha o menu
             * })
             * </pre>
             * @return {object} Retorna um objeto correspondente a uma Factory
             **/
            this.$get = this.get = /*@ngInject*/ function($rootScope, $mdSidenav) {
                return {
                    main: this.mainMenu,
                    toolbar: this.toolbarMenu,
                    api: api(instance, $rootScope, $mdSidenav)
                }
            }
            /**
             * @ngdoc function
             * @name core.menu.$menuProvider#set
             * @methodOf core.menu.$menuProvider
             * @description
             * Adicionar um novo menu
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($menuProvider) {
             *     $menuProvider.set({
             *         name: 'Conta',
             *         type: 'link',
             *         icon: 'fa fa-at',
             *         url: '/account/',
             *         state: 'app.account'
             *     });
             * })
             * </pre>
             * @param {object} menu objeto contendo as propriedades do menu
             **/
            this.set = function(menu) {
                this.mainMenu.push(menu);
            }
            /**
             * @ngdoc function
             * @name core.menu.$menuProvider#setToolbar
             * @methodOf core.menu.$menuProvider
             * @description
             * Adicionar um novo menu no toolbar
             * @example
             * <pre>
             * angular.module('myApp.module').config(function($menuProvider) {
             *     $menuProvider.setToolbar({
             *         name: 'Conta',
             *         type: 'link',
             *         icon: 'fa fa-at',
             *         url: '/account/',
             *         state: 'app.account'
             *     });
             * })
             * </pre>
             * @param {object} menu objeto contendo as propriedades do menu
             **/
            this.setToolbar = function(menu) {
                this.toolbarMenu.push(menu);
            }
            /**
             * @ngdoc function
             * @name core.menu.$menuProvider#api
             * @methodOf core.menu.$menuProvider
             * @example
             * <pre>
             * angular.module('myApp.module').controller('MyCtrl', function($menu) {
             *      $menu.api().open() //abre o menu
             *      $menu.api().close() //fecha o menu
             * })
             * </pre>
             * @return {object} comportamentos do menu
             **/
            function api(instance, $rootScope, $mdSidenav) {
                return function api() {
                    return {
                        openedSection: false,
                        currentPage: null,
                        open: function() {
                            $rootScope.$emit('AppMenuOpened');
                            $mdSidenav('left').open();
                        },
                        close: function() {
                            $rootScope.$emit('AppMenuClosed');
                            $mdSidenav('left').close();
                        },
                        //sections: sampleMenu(),
                        sections: instance.mainMenu,
                        selectSection: function(section) {
                            this.openedSection = section;
                        },
                        toggleSelectSection: function(section) {
                            this.openedSection = (this.openedSection === section ? false : section);
                        },
                        isChildSectionSelected: function(section) {
                            return this.openedSection === section;
                        },
                        isSectionSelected: function(section) {
                            var selected = false;
                            var openedSection = this.openedSection;
                            if (openedSection === section) {
                                selected = true;
                            } else if (section.children) {
                                section.children.forEach(function(childSection) {
                                    if (childSection === openedSection) {
                                        selected = true;
                                    }
                                });
                            }
                            return selected;
                        },
                        selectPage: function(section, page) {
                            //page && page.url && $location.path(page.url);
                            this.currentSection = section;
                            this.currentPage = page;
                        },
                        isPageSelected: function(page) {
                            return this.currentPage === page;
                        },
                        isOpen: function(section) {
                            return this.isSectionSelected(section);
                        },
                        toggleOpen: function(section) {
                            return this.toggleSelectSection(section);
                        },
                        isSelected: function(page) {
                            return this.isPageSelected(page);
                        }
                    }
                }
            }
            //
            // MENU SECTIONS SAMPLE
            //
            /*function sampleMenu() {
                return [
                    {
                    name: 'API Reference',
                    type: 'heading',
                    //iconSvg: 'ic_dashboard',
                    icon: 'fa fa-dashboard',
                    children: [{
                        name: 'Layout',
                        type: 'toggle',
                        pages: [{
                            name: 'Container Elements',
                            id: 'layoutContainers',
                            url: '/layout/container'
                        }, {
                            name: 'Grid System',
                            id: 'layoutGrid',
                            url: '/layout/grid'
                        }, {
                            name: 'Child Alignment',
                            id: 'layoutAlign',
                            url: '/layout/alignment'
                        }, {
                            name: 'Options',
                            id: 'layoutOptions',
                            url: '/layout/options'
                        }]
                    }, {
                        name: 'Services',
                        pages: [],
                        type: 'toggle'
                    }, {
                        name: 'Directives',
                        pages: [],
                        type: 'toggle'
                    }]
                }*/
            /*  {
                    name: 'Finalizar pedido',
                    url: '/checkout',
                    type: 'link'
                }, 
*/
            /*{
                        name: 'Produtos',
                        type: 'toggle',
                        icon: 'fa fa-diamond',
                        pages: [{
                            name: 'Pulseira A',
                            id: 'pulseira-a',
                            url: '/produtos/pulseira-a'
                        }, {
                            name: 'Pulseira B',
                            id: 'pulseira-b',
                            url: '/produtos/pulseira-b'
                        }, {
                            name: 'Pulseira C',
                            id: 'pulseira-c',
                            url: '/produtos/pulseira-c'
                        }]

                    }
                    ,
                    {
                        name: 'Políticas',
                        type: 'toggle',
                        icon: 'fa fa-file-text-o',
                        pages: [{
                            name: 'Termo de compromisso',
                            id: 'termo',
                            url: '/termo'
                        }, {
                            name: 'Política de privacidade',
                            id: 'privacidade',
                            url: '/privacidade'
                        }, ]
                    }, {
                        name: 'Sobre',
                        type: 'toggle',
                        icon: 'fa fa-briefcase',
                        pages: [{
                            name: 'Quem somos',
                            id: 'quemSomos',
                            url: '/quem-somos'
                        }]
                    }
                ];
            }*/
        })

})();
(function() {
    'use strict';
    angular.module('core.menu').filter('menuHuman', /*@ngInject*/ function menuHuman() {
        return function(doc) {
            if (!doc) return;
            if (doc.type === 'directive') {
                return doc.name.replace(/([A-Z])/g, function($1) {
                    return '-' + $1.toLowerCase();
                });
            }
            return doc.label || doc.name;
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.menu').controller('MenuLinkCtrl', /*@ngInject*/ function($scope, $state, $location, $mdSidenav, $anchorScroll, $timeout) {
        var vm = this;
        vm.state = $state;
        vm.goAndClose = function(route, state, anchor, event, section) {
            if (!state || !anchor) defaultAction(route);

            if (state && anchor) scrollToAction(state, anchor, event, section);
        }

        function defaultAction(route) {
            //
            // Transition to path
            // 
            $location.path(route);

            //
            // Close sidenav
            // 
            $mdSidenav('left').close();
        }

        function scrollToAction(state, anchor, event, section) {
            var currentState = $state.current.name;

            // 
            // Override state to go if anchorSelf is set
            // 
            state = section.anchorSelf ? currentState : state;
            
            if (state == currentState) {
                event.stopPropagation();
                var off = $scope.$on('$locationChangeStart', function(ev) {
                    off();
                    ev.preventDefault();
                });

                scroll(anchor);
            } else {
                $state.go(state);
                $scope.$on('$stateChangeSuccess', function(ev) {

                    $timeout(function() {
                        event.stopPropagation();
                        var off = $scope.$on('$locationChangeStart', function(ev) {
                            off();
                            ev.preventDefault();
                        });

                        scroll(anchor);
                    }, 2000);
                });
            }
        }

        function scroll(anchor) {
            //
            // Close sidenav
            // 
            $mdSidenav('left').close();

            $timeout(function() {
                $location.hash(anchor);
                $anchorScroll();
            }, 500);
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.menu').directive('menuLink', /*@ngInject*/ function() {
        return {
            scope: {
                section: '='
            },
            controller: 'MenuLinkCtrl',
            controllerAs: 'vm',
            templateUrl: 'core/page/menu/menuLink.tpl.html',
            link: function($scope, $element) {
                var controller = $element.parent().controller();
                $scope.isSelected = function() {
                    return controller.menu ? controller.menu.isSelected($scope.section) : '';
                };
            }
        };
    });
})();
(function() {
    'use strict';
    angular.module('core.menu').directive('menuToggle', /*@ngInject*/ function() {
        return {
            scope: {
                section: '='
            },
            templateUrl: 'core/page/menu/menuToggle.tpl.html',
            link: function($scope, $element) {
                var controller = $element.parent().controller();
                $scope.isOpen = function() {
                    return controller.menu.isOpen($scope.section);
                };
                $scope.toggle = function() {
                    controller.menu.toggleOpen($scope.section);
                };
                var parentNode = $element[0].parentNode.parentNode.parentNode;
                if (parentNode.classList.contains('parent-list-item')) {
                    var heading = parentNode.querySelector('h2');
                    $element[0].firstChild.setAttribute('aria-describedby', heading.id);
                }
            }
        }
    });
})();
(function() {
    'use strict';
    angular.module('core.menu').filter('nospace', /*@ngInject*/ function() {
        return function(value) {
            return (!value) ? '' : value.replace(/ /g, '');
        }
    });
})();
(function() {
    'use strict';
    /* global moment */
    /**
     * @ngdoc filter
     * @name core.utils.filter:age
     * @description
     * Filtro para converter data (EN) para idade
     * @param {date} value data de nascimento
     * @example
     * <pre>
     * {{some_date | age}}
     * </pre>
     **/
    angular.module('core.utils').filter('age', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return '';
            return moment(value).fromNow(true);
        };
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc filter
     * @name core.utils.filter:cep
     * @description
     * Filtro para adicionar máscara de CEP
     * @param {string} value código postal
     * @example
     * <pre>
     * {{some_text | cep}}
     * </pre>
     **/
    angular.module('core.utils').filter('cep', /*@ngInject*/ function() {
        return function(input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/^(\d{2})(\d{3})(\d)/, '$1.$2-$3');
            return str;
        }
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc filter
     * @name core.utils.filter:cnpj
     * @description
     * Filtro para adicionar máscara de CNPJ
     * @param {string} value CNPJ
     * @example
     * <pre>
     * {{some_text | cnpj}}
     * </pre>
     **/
    angular.module('core.utils').filter('cnpj', /*@ngInject*/ function() {
        return function(input) {
            // regex créditos @ Matheus Biagini de Lima Dias
            var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/^(\d{2})(\d)/, '$1.$2');
            str = str.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
            str = str.replace(/\.(\d{3})(\d)/, '.$1/$2');
            str = str.replace(/(\d{4})(\d)/, '$1-$2');
            return str;
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc filter
     * @name core.utils.filter:cpf
     * @description
     * Filtro para adicionar máscara de CPF
     * @param {string} value CPF
     * @example
     * <pre>
     * {{some_text | cpf}}
     * </pre>
     **/
    angular.module('core.utils').filter('cpf', /*@ngInject*/ function() {
        return function(input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            str = str.replace(/(\d{3})(\d)/, '$1.$2');
            str = str.replace(/(\d{3})(\d)/, '$1.$2');
            str = str.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
            return str;
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc filter
     * @name core.utils.filter:cut
     * @description
     * Filtro para cortar strings e adicionar "..."
     * @param {string} value palavra ou texto
     * @param {bool} wordwise cortar por palavras
     * @param {integer} max tamanho máximo do corte
     * @param {string} tail final da string (cauda)
     * @example
     * <pre>
     * {{some_text | cut:true:100:' ...'}}
     * </pre>
     **/
    angular.module('core.utils').filter('cut', /*@ngInject*/ function() {
        return function(value, wordwise, max, tail) {
            if (!value) return '';
            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;
            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }
            return value + (tail || ' …');
        };
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc filter
     * @name core.utils.filter:phone
     * @description
     * Adicionar máscara de telefone
     * @param {string} value telefone
     * @example
     * <pre>
     * {{some_text | phone}}
     * </pre>
     **/
    angular.module('core.utils').filter('phone', /*@ngInject*/ function() {
        return function(input) {
            var str = input + '';
            str = str.replace(/\D/g, '');
            if (str.length === 11) {
                str = str.replace(/^(\d{2})(\d{5})(\d{4})/, '($1) $2-$3');
            } else {
                str = str.replace(/^(\d{2})(\d{4})(\d{4})/, '($1) $2-$3');
            }
            return str;
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc filter
     * @name core.utils.filter:randomInteger
     * @description
     * Converter para um número random
     * @param {integer} value valor corrente
     * @param {integer} min valor mínimo
     * @param {integer} max valor máximo
     * @example
     * <pre>
     * {{some_number | randomInteger:1:10}}
     * </pre>
     **/
    angular.module('core.utils').filter('randomInteger', /*@ngInject*/ function() {
        return function(value, min, max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc filter
     * @name core.utils.filter:real
     * @description
     * Adicionar mascára de moeda no formato real (BR)
     * @param {string} value valor
     * @param {bool} prefix prefixo R$
     * @example
     * <pre>
     * {{some_text | real:true}}
     * </pre>
     **/
    angular.module('core.utils').filter('real', /*@ngInject*/ function() {
        return function(input, prefix) {
            return prefix ? 'R$ ' : '' + formatReal(input);
        }

        function formatReal(int) {
            var tmp = int + '';
            var res = tmp.replace('.', '');
            tmp = res.replace(',', '');
            var neg = false;
            if (tmp.indexOf('-') === 0) {
                neg = true;
                tmp = tmp.replace('-', '');
            }
            if (tmp.length === 1) {
                tmp = '0' + tmp;
            }
            tmp = tmp.replace(/([0-9]{2})$/g, ',$1');
            if (tmp.length > 6) {
                tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, '.$1,$2');
            }
            if (tmp.length > 9) {
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, '.$1.$2,$3');
            }
            if (tmp.length > 12) {
                tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g, '.$1.$2.$3,$4');
            }
            if (tmp.indexOf('.') === 0) {
                tmp = tmp.replace('.', '');
            }
            if (tmp.indexOf(',') === 0) {
                tmp = tmp.replace(',', '0,');
            }
            return neg ? '-' + tmp : tmp;
        }
    });
})();
(function() {
    'use strict';
    //
    // Usage:
    // {{some_array | slice:start:end }}
    //
    angular.module('core.utils').filter('slice', /*@ngInject*/ function sliceFilter() {
        return function(arr, start, end) {
            return arr.slice(start, end);
        };
    })
})();
(function() {
    'use strict';
    //
    // Usage:
    // {{some_date | title }}
    //
    angular.module('core.utils').filter('title', /*@ngInject*/ function titleFilter() {
        return function(input) {
            input = input || '';
            return input.replace(/\w\S*/g, function(txt) {
                var whitelist = ['I', 'II'];
                if (txt.length > 2) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                } else {
                    if (whitelist.indexOf(txt.toUpperCase()) >= 0) {
                        return txt.toUpperCase();
                    } else {
                        return txt.toLowerCase();
                    }
                }
            });
        }
    })
})();
(function() {
    'use strict';
    //
    // Usage:
    // {{months | toYears }}
    //
    angular.module('core.utils').filter('toYears', /*@ngInject*/ function() {
        return function(value) {
            if (!value) return '';
            var what = value,
                stringYear = '',
                stringMonth = '';
            if (value >= 12) {
                what = value / 12;
                if (isFloat(what)) {
                    var real = what, //1.25
                        years = parseInt(real), //1
                        months = Math.floor((real - years) * 10);
                    if (years)
                        stringYear = years + ' ano' + ((years > 1) ? 's' : '');
                    if (months)
                        stringMonth = ' e ' + months + ' mes' + ((months > 1) ? 'es' : '');
                } else {
                    stringYear = what + ' ano' + ((what > 1) ? 's' : '');
                }
            } else {
                stringMonth = value + ' mes' + ((value > 1) ? 'es' : '');
            }
            return stringYear + stringMonth;
        }

        function isFloat(n) {
            return n === Number(n) && n % 1 !== 0
        }
    })
})();
(function() {
    'use strict';
    //
    // Usage:
    // {{some_str | unsafe }}
    //
    angular.module('core.utils').filter('unsafe', /*@ngInject*/ function($sce) {
        return function(value) {
            return $sce.trustAsHtml(value);
        };
    })
})();
(function() {
    'use strict';
    angular.module('core.utils').factory('HttpInterceptor', /*@ngInject*/ function($q, $rootScope) {
        return {
            // optional method
            'request': function(config) {
                // do something on success
                return config;
            },
            // optional method
            'requestError': function(rejection) {
                // do something on error
                //if (canRecover(rejection)) {
                //return responseOrNewPromise
                //}
                return $q.reject(rejection);
            },
            // optional method
            'response': function(response) {
                // do something on success
                return response;
            },
            // optional method
            'responseError': function(rejection) {
                if (rejection.status === 401 || rejection.status === 403) {
                    $rootScope.$emit('$Unauthorized', rejection.status);
                }
                // do something on error
                //if (canRecover(rejection)) {
                //return responseOrNewPromise
                //}
                return $q.reject(rejection);
            }
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.page').controller('ToolbarMenuCtrl', /*@ngInject*/ function($rootScope, $mdBottomSheet) {
        var vm = this;
        $rootScope.$on('AppMenuOpened', function() {
            $mdBottomSheet.hide();
        });
        $rootScope.$on('CompanyIdUpdated', function() {
            $mdBottomSheet.hide();
        });
        vm.showFilters = function() {
            $mdBottomSheet.show({
                templateUrl: 'app/finder/filter/finderFilterMobile.tpl.html',
                controller: 'FinderFilterCtrl',
                controllerAs: 'vm',
                //targetEvent: $event,
                //parent: '.finder-wrapper',
                locals: {},
                //scope: ''
                //preserveScope: true,
                disableParentScroll: false
            }).then(function() {});
        }
        //
        // Events
        //
        //
        // Bootstrap
        //
        //
        bootstrap();

        function bootstrap() {}
    })
})();
(function() {
    'use strict';
    angular.module('core.page').directive('toolbarMenu', /*@ngInject*/ function toolbarMenu($menu) {
        return {
            templateUrl: "core/page/toolbar/menu/toolbarMenu.tpl.html",
            scope: {
                company: '='
            },
            controller: 'ToolbarMenuCtrl',
            controllerAs: 'vm',
            link: function(scope) {
                scope.menu = $menu.toolbar;
            }
        }
    })
})();
(function() {
    'use strict';
    angular.module('core.page').directive('toolbarTitle', /*@ngInject*/ function($app) {
        return {
            templateUrl: function() {
                return $app.toolbarTitleUrl;
            }
        }
    });
})();
(function() {
    'use strict';
    angular.module('core.utils').controller('CeperCtrl', /*@ngInject*/ function($scope, $http, $page) {
        var vm = this;
        vm.busy = false;
        vm.get = get;

        if (!$scope.address || typeof $scope.address != 'object')
            $scope.address = {};

        function get() {
            var cep = $scope.ngModel;
            if (cep && cep.toString().length === 8) {
                var url = $scope.endpointUrl
                vm.busy = true;
                var onSuccess = function(response) {
                    vm.busy = false;
                    var addr = response.data;
                    if (addr.city || addr.state) {
                        $scope.address.street = addr.street;
                        $scope.address.district = addr.district;
                        $scope.address.city = addr.city;
                        $scope.address.state = addr.state;
                    } else {
                        error();
                    }
                }
                var onError = function(response) {
                    vm.busy = false;
                    error();
                }
                $http.get(url + cep, {}).then(onSuccess, onError);
            }

            function error() {
                return $page.toast('Não foi possível encontrar o endereço com este cep, por favor insira manualmente', 10000, 'top right');
            }
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.utils.directive:ceper
     * @restrict EA
     * @description
     * Input para auto busca de cep
     * @element div
     * @param {object} ngModel model qye representa o campo numerico do cep
     * @param {object} address model que representa os campos de endereço (street, district, city, state)
     * @param {string} endpointUrl endereço do server que deverá responder o json no formato esperado
     **/
    angular.module('core.utils').directive('ceper', /*@ngInject*/ function() {
        return {
            scope: {
                ngModel: '=',
                address: '=',
                endpointUrl: '@'
            },
            replace: true,
            restrict: 'EA',
            controller: 'CeperCtrl',
            controllerAs: 'vm',
            templateUrl: 'core/utils/directives/ceper/ceper.tpl.html'
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc object
     * @name core.utils.controller:ContactFormCtrl
     * @description
     * @requires $scope
     * @requires $http
     * @requires $page
     * @requires api
     **/
    angular.module('core.utils').controller('ContactFormCtrl', /*@ngInject*/ function($scope, $http, $page, api) {
        var vm = this;
        vm.save = save;
        vm.busy = false;
        if (!$scope.handleForm)
            $scope.handleForm = {};

        $scope.$watch('ngModel', function(nv, ov) {
            if (nv != ov) {
                $scope.handleForm.$dirty = true;
            }
        }, true)

        function save() {
            if ($scope.endpointUrl) {
                vm.busy = true;
                $http
                    .put($scope.endpointUrl, $scope.ngModel)
                    .success(function(response) {
                        vm.busy = false;
                        $scope.handleForm.$dirty = false;
                        $page.toast('Seu contato foi atualizado');
                        if ($scope.callbackSuccess && typeof $scope.callbackSuccess === 'function')
                            $scope.callbackSuccess(response);
                    })
                    .error(function() {
                        vm.busy = false;
                    });
            }
        }
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.utils.directive:contactForm
     * @restrict EA
     * @description
     * Componente para formularios de contato
     * @element
     * div
     * @param {object} ngModel model do contato
     * @param {object} handleForm model para reter estados do form
     * @param {string} endpointUrl endereço do servidor api
     * @param {function} callbackSuccess callback de sucesso
     **/
    angular.module('core.utils').directive('contactForm', /*@ngInject*/ function() {
        return {
            scope: {
                ngModel: '=',
                handleForm: '=',
                endpointUrl: '@',
                callbackSuccess: '='
            },
            controller: 'ContactFormCtrl',
            controllerAs: 'vm',
            templateUrl: 'core/utils/directives/contactForm/contactForm.tpl.html',
            replace: true,
            restrict: 'EA'
        }
    })
})();
(function() {
    'use strict';
    //https://github.com/sparkalow/angular-count-to
    angular.module('core.utils').directive('countTo', /*@ngInject*/ function($timeout) {
        return {
            replace: false,
            scope: true,
            link: function(scope, element, attrs) {
                var e = element[0];
                var num, refreshInterval, duration, steps, step, countTo, value, increment;
                var calculate = function() {
                    refreshInterval = 30;
                    step = 0;
                    scope.timoutId = null;
                    countTo = parseInt(attrs.countTo) || 0;
                    scope.value = parseInt(attrs.value, 10) || 0;
                    duration = (parseFloat(attrs.duration) * 1000) || 0;
                    steps = Math.ceil(duration / refreshInterval);
                    increment = ((countTo - scope.value) / steps);
                    num = scope.value;
                }
                var tick = function() {
                    scope.timoutId = $timeout(function() {
                        num += increment;
                        step++;
                        if (step >= steps) {
                            $timeout.cancel(scope.timoutId);
                            num = countTo;
                            e.textContent = countTo;
                        } else {
                            e.textContent = Math.round(num);
                            tick();
                        }
                    }, refreshInterval);
                }
                var start = function() {
                    if (scope.timoutId) {
                        $timeout.cancel(scope.timoutId);
                    }
                    calculate();
                    tick();
                }
                attrs.$observe('countTo', function(val) {
                    if (val) {
                        start();
                    }
                });
                attrs.$observe('value', function(val) {
                    start();
                });
                return true;
            }
        }
    });
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.utils.directive:imageCutter
     * @restrict EA
     * @description
     * Cortador de imagens praparado para trabalhar com backend
     * @element a
     * @param {string} endpointUrl endereço do server
     * @param {object} endpointParams parâmetros adicionais enviado ao server
     * @param {function} endpointSuccess callback para sucesso
     * @param {function} endpointFail callback para falha
     * @param {bool} cutOnModal utilizar em modo md-dialog
     * @param {string} cutOnModalTitle título do md-dialog
     * @param {integer} cutWidth tamanho do corte
     * @param {integer} cutHeight altura do corte
     * @param {string} cutShape formato do corte (circle|square)
     * @param {string} cutLabel nome do botão cortar
     * @param {var} cutResult resultado do corte (base64)
     * @param {var} cutStep passo atual do corte (1|2|3)
     **/

    angular.module('core.utils').directive('imageCutter', /*@ngInject*/ function($mdDialog, $http, $rootScope) {
        return {
            scope: {
                endpointUrl: '@',
                endpointParams: '=',
                endpointSuccess: '=',
                endpointFail: '=',
                cutOnModal: '@',
                cutOnModalTitle: '@',
                cutWidth: '@',
                cutHeight: '@',
                cutShape: '@',
                cutLabel: '@',
                cutResult: '=',
                cutStep: '=',
                cutText: '@'
            },
            replace: true,
            transclude: true,
            restrict: 'EA',
            templateUrl: 'core/utils/directives/imageCutter/imageCutter.tpl.html',
            controller: 'ImageCutterAreaCtrl',
            link: function($scope, $elem) {
                $scope.modal = modal;
                $scope.hide = hide;
                $scope.$watch('cutStep', function(nv, ov) {
                    if (nv != ov) {
                        /**
                         * Passo 3 - corte
                         */
                        if (nv === 3) {
                            /**
                             * Enviar para o server
                             */
                            if ($scope.cutOnModal)
                                $scope.send()
                        }
                    }
                })

                function modal(ev) {
                    //
                    // reset
                    //
                    reboot();
                    //
                    // open dialog
                    //
                    $mdDialog.show({
                        templateUrl: 'core/utils/directives/imageCutter/modal.tpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        scope: $scope, // use parent scope in template
                        preserveScope: true
                    });
                }

                function hide() {
                    $mdDialog.hide();
                }

                function toggleOpacity() {
                    $scope.$emit('ImageCutterToggleOpacity')
                }

                function toggleBusy() {
                    $scope.$emit('ImageCutterToggleBusy')
                }

                function reboot() {
                    $scope.$emit('ImageCutterReboot')
                }
            }
        }
    });
})();
(function() {
    'use strict';
    angular.module('core.utils').controller('ToolbarAvatarCtrl', /*@ngInject*/ function($location, $timeout) {
        var vm = this;
        vm.logout = logout;

        function logout() {
            $timeout(function() {
                $location.path('/logout/');
            }, 1200);
        }
    })
})();
(function() {
    'use strict';
    /**
     * @ngdoc directive
     * @name core.utils.directive:toolbarAvatar
     * @restrict EA
     * @description
     * Avatar com menu para o md-toolbar
     * @element a
     * @param {string} firstName primeiro nome
     * @param {string} email email
     * @param {string} facebook id do facebook
     * @param {array} menu lista de itens do menu
     **/
    angular.module('core.utils').directive('toolbarAvatar', /*@ngInject*/ function() {
        return {
            scope: {
                firstName: '@',
                email: '@',
                facebook: '@',
                menu: '='
            },
            replace: true,
            //transclude: true,
            restrict: 'EA',
            templateUrl: 'core/utils/directives/toolbarAvatar/toolbarAvatar.tpl.html',
            controller: 'ToolbarAvatarCtrl',
            controllerAs: 'vm'
        }
    });
})();
(function() {
    'use strict';
    angular.module('core.utils').controller('ImageCutterAreaCtrl', /*@ngInject*/ function($scope, $http, $mdDialog) {
        $scope.send = function() {
            if ($scope.endpointUrl) {
                toggleBusy();
                //colocando em um intervalo de tempo pra pegar corretamente o resultado do cut
                var interval = setInterval(function() {
                    var params = {
                            image: $scope.cutResult
                        }
                        //extendendo aos parametros da diretiva
                    angular.extend(params, $scope.endpointParams);
                    //send to server
                    $http
                        .put($scope.endpointUrl, params)
                        .success(function(response) {
                            if (typeof $scope.endpointSuccess === 'function') $scope.endpointSuccess(response);
                            toggleBusy();
                            if ($scope.cutOnModal) {
                                $mdDialog.hide();
                            }
                        })
                        .error(function(response) {
                            if (typeof $scope.endpointFail === 'function') $scope.endpointFail(response);
                            toggleBusy();
                        })
                    //limpando intervalo de tempo pra não gerar loop infinito
                    clearInterval(interval);
                }, 1000);
            }
        }

        function toggleOpacity() {
            $scope.$emit('ImageCutterToggleOpacity')
        }

        function toggleBusy() {
            $scope.$emit('ImageCutterToggleBusy')
        }

        function reboot() {
            $scope.$emit('ImageCutterReboot')
        }

    })
})();
(function() {
    'use strict';
    angular.module('core.utils').directive('imageCutterArea', /*@ngInject*/ function($http, $compile, $rootScope, $mdDialog) {
        return {
            scope: {
                endpointUrl: '@',
                endpointParams: '=',
                endpointSuccess: '=',
                endpointFail: '=',
                cutOnModal: '@',
                cutWidth: '@',
                cutHeight: '@',
                cutShape: '@',
                cutLabel: '@',
                cutResult: '=',
                cutStep: '='
            },
            replace: true,
            //transclude: true,
            restrict: 'EA',
            controller: 'ImageCutterAreaCtrl',
            // controllerAs: 'vm',
            templateUrl: 'core/utils/directives/imageCutter/area/imageCutterArea.tpl.html',
            link: function($scope, $elem, $attr) {
                $scope.cutLabel = $scope.cutLabel ? $scope.cutLabel : 'Crop';
                $scope.endpointParams = $scope.endpointParams ? $scope.endpointParams : {};
                $scope.reboot = reboot;
                $scope.$watch('cutStep', function(nv, ov) {
                    if (nv != ov) {
                        /**
                         * Passo 2 - seleção da imagem
                         */
                        if (nv === 2) {
                            //add material classes and icon to "crop" button
                            $($elem).find('button:contains("Crop")')
                                .addClass('md-raised md-primary md-button md-default-theme')
                                .html('<span><i class="fa fa-crop"></i> ' + $scope.cutLabel + '<span>')
                            //coloca o bottao de reset ao lado do bottao de crop
                            .parent()
                                .append($($elem).find('button.refresh').removeAttr('ng-transclude'))
                                .parent()
                                .prepend($($elem).find('div.progress'));

                            // var interval = setInterval(function() {
                            //     $scope.$apply(function() {
                            //         clearInterval(interval);
                            //     })
                            // }, 500);
                        }
                        /**
                         * Passo 3 - corte
                         */
                        if (nv === 3) {
                            /**
                             * Enviar para o server
                             */

                            if (!$scope.cutOnModal || $scope.cutOnModal == 'false')
                                $scope.send();
                        }


                    }
                })
                $rootScope.$on('ImageCutterToggleOpacity', function() {
                    toggleOpacity();
                })

                $rootScope.$on('ImageCutterReboot', function() {
                    reboot();
                })

                $rootScope.$on('ImageCutterToggleBusy', function() {
                    toggleBusy();
                })

                function toggleOpacity() {
                    $($elem).find('img.image-crop-final').toggleClass('opacity-3');
                }

                function toggleBusy() {
                    $scope.busy = !$scope.busy;
                    if ($scope.busy === false) {
                        //$compile($elem)($scope)
                        //re-reboot directive
                        reboot();
                    }
                    toggleOpacity();
                }

                function reboot() {
                    $scope.cutResult = null;
                    $scope.cutStep = 1;
                    $($elem).find('input.image-crop-input').val('');
                    $rootScope.$emit('CropReset');
                }
            }
        }
    });
})();
angular.module("app.kit").run(["$templateCache", function($templateCache) {$templateCache.put("core/home/home.tpl.html","<div class=\"main-wrapper anim-zoom-in md-padding home\" layout=\"column\" flex=\"\"><div class=\"text-center\">Olá moda foca <a ui-sref=\"app.login\">entrar</a></div></div>");
$templateCache.put("core/login/login.tpl.html","<md-content class=\"md-padding anim-zoom-in login\" layout=\"row\" layout-sm=\"column\" ng-if=\"!app.isAuthed()\" flex=\"\"><div layout=\"column\" class=\"login\" layout-padding=\"\" flex=\"\"><login-form config=\"vm.config\" user=\"app.user\"></login-form></div></md-content>");
$templateCache.put("core/login/facebook/facebookLogin.tpl.html","<button flex=\"\" ng-click=\"fb.login()\" ng-disabled=\"app.$page.load.status\" layout=\"row\"><i class=\"fa fa-facebook\"></i> <span>Entrar com Facebook</span></button>");
$templateCache.put("core/login/form/loginForm.tpl.html","<div class=\"wrapper md-whiteframe-z1\"><img class=\"avatar\" src=\"assets/images/avatar-m.jpg\"><md-content class=\"md-padding\"><form name=\"logon\" novalidate=\"\"><div layout=\"row\" class=\"email\"><i class=\"fa fa-at\"></i><md-input-container flex=\"\"><label>Email</label> <input ng-model=\"logon.email\" type=\"email\" required=\"\"></md-input-container></div><div layout=\"row\" class=\"senha\"><i class=\"fa fa-key\"></i><md-input-container flex=\"\"><label>Senha</label> <input ng-model=\"logon.password\" type=\"password\" required=\"\"></md-input-container></div></form></md-content><div layout=\"row\" layout-padding=\"\"><button flex=\"\" class=\"entrar\" ng-click=\"vm.login(logon)\" ng-disabled=\"logon.$invalid||app.$page.load.status\">Entrar</button><facebook-login user=\"user\"></facebook-login></div></div><div class=\"help\" layout=\"row\"><a flex=\"\" ui-sref=\"app.login-lost\" class=\"lost\"><i class=\"fa fa-support\"></i> Esqueci minha senha</a> <a flex=\"\" ui-sref=\"app.signup\" class=\"lost\"><i class=\"fa fa-support\"></i> Não tenho cadastro</a></div><style>\nbody, html {  overflow: auto;}\n</style>");
$templateCache.put("core/login/google/googleLogin.tpl.html","<google-plus-signin clientid=\"{{google.clientId}}\" language=\"{{google.language}}\"><button class=\"google\" layout=\"row\" ng-disabled=\"app.$page.load.status\"><i class=\"fa fa-google-plus\"></i> <span>Entrar com Google</span></button></google-plus-signin>");
$templateCache.put("core/login/register/lost.tpl.html","<div layout=\"row\" class=\"login-lost\" ng-if=\"!app.isAuthed()\"><div layout=\"column\" class=\"login\" flex=\"\" ng-if=\"!vm.userHash\"><div class=\"wrapper md-whiteframe-z1\"><img class=\"avatar\" src=\"assets/images/avatar-m.jpg\"><md-content class=\"md-padding\"><form name=\"lost\" novalidate=\"\"><div layout=\"row\" class=\"email\"><i class=\"fa fa-at\"></i><md-input-container flex=\"\"><label>Email</label> <input ng-model=\"email\" type=\"email\" required=\"\"></md-input-container></div></form></md-content><md-button class=\"md-primary md-raised entrar\" ng-disabled=\"lost.$invalid||app.$page.load.status\" ng-click=\"!lost.$invalid?vm.lost(email):false\">Recuperar</md-button></div></div><div layout=\"column\" class=\"login\" flex=\"\" ng-if=\"vm.userHash\"><div class=\"wrapper md-whiteframe-z1\"><img class=\"avatar\" src=\"assets/images/avatar-m.jpg\"><h4 class=\"text-center\">Entre com sua nova senha</h4><md-content class=\"md-padding\"><form name=\"lost\" novalidate=\"\"><div layout=\"row\" class=\"email\"><i class=\"fa fa-key\"></i><md-input-container flex=\"\"><label>Senha</label> <input ng-model=\"senha\" type=\"password\" required=\"\"></md-input-container></div><div layout=\"row\" class=\"email\"><i class=\"fa fa-key\"></i><md-input-container flex=\"\"><label>Repetir senha</label> <input ng-model=\"senhaConfirm\" name=\"senhaConfirm\" type=\"password\" match=\"senha\" required=\"\"></md-input-container></div></form></md-content><md-button class=\"md-primary md-raised entrar\" ng-disabled=\"lost.$invalid||app.$page.load.status\" ng-click=\"!lost.$invalid?vm.change(senha):false\">Alterar</md-button></div><div ng-show=\"lost.senhaConfirm.$error.match\" class=\"warn\"><span>(!) As senhas não conferem</span></div></div></div><style>\nbody, html {  overflow: auto;}\n</style>");
$templateCache.put("core/login/register/register.tpl.html","<md-content class=\"md-padding anim-zoom-in login\" layout=\"row\" layout-sm=\"column\" ng-if=\"!app.isAuthed()\" flex=\"\"><div layout=\"column\" class=\"register\" layout-padding=\"\" flex=\"\"><register-form config=\"vm.config\"></register-form></div></md-content>");
$templateCache.put("core/login/register/registerForm.tpl.html","<div class=\"wrapper md-whiteframe-z1\"><img class=\"avatar\" src=\"assets/images/avatar-m.jpg\"><md-content><form name=\"registerForm\" novalidate=\"\"><div layout=\"row\" layout-sm=\"column\" class=\"nome\"><i hide-sm=\"\" class=\"fa fa-smile-o\"></i><md-input-container flex=\"\"><label>Seu nome</label> <input ng-model=\"sign.firstName\" type=\"text\" required=\"\"></md-input-container><md-input-container flex=\"\"><label>Sobrenome</label> <input ng-model=\"sign.lastName\" type=\"text\" required=\"\"></md-input-container></div><div layout=\"row\" class=\"email\"><i class=\"fa fa-at\"></i><md-input-container flex=\"\"><label>Email</label> <input ng-model=\"sign.email\" type=\"email\" required=\"\"></md-input-container></div><div layout=\"row\" class=\"senha\"><i class=\"fa fa-key\"></i><md-input-container flex=\"\"><label>Senha</label> <input ng-model=\"sign.password\" type=\"password\" required=\"\"></md-input-container></div></form><div layout=\"row\" layout-padding=\"\"><button flex=\"\" class=\"entrar\" ng-disabled=\"registerForm.$invalid||app.$page.load.status\" ng-click=\"register(sign)\">Registrar</button><facebook-login user=\"user\"></facebook-login></div></md-content></div><div layout=\"column\"><a flex=\"\" class=\"lost\" ui-sref=\"app.pages({slug:\'terms\'})\"><i class=\"fa fa-warning\"></i> Concordo com os termos</a></div><style>\nbody, html {  overflow: auto;}\n</style>");
$templateCache.put("core/page/layout/layout.tpl.html","<md-sidenav ui-view=\"sidenav\" class=\"page-menu md-sidenav-left md-whiteframe-z2\" md-component-id=\"left\" md-is-locked-open=\"$mdMedia(\'gt-md\')\" ng-if=\"app.isAuthed()\"></md-sidenav><div layout=\"column\" flex=\"\" class=\"main-content-wrapper\"><loader></loader><md-toolbar ui-view=\"toolbar\" class=\"main\" ng-class=\"{\'not-authed\':!app.isAuthed()&&!app.user.current(\'company\')}\" md-scroll-shrink=\"\" md-shrink-speed-factor=\"0.25\"></md-toolbar><md-content class=\"main-content\" on-scroll-apply-opacity=\"\"><div ui-view=\"content\" ng-class=\"{ \'anim-in-out anim-slide-below-fade\': app.state.current.name != \'app.profile\' && app.state.current.name != \'app.landing\'}\"></div></md-content></div>");
$templateCache.put("core/page/loader/loader.tpl.html","<div class=\"page-loader\" ng-class=\"{\'show\':app.$page.load.status}\"><md-progress-linear md-mode=\"indeterminate\"></md-progress-linear></div>");
$templateCache.put("core/page/menu/menuLink.tpl.html","<md-button ng-class=\"{\'active\' : isSelected()||vm.state.current.name === section.state}\" ng-click=\"vm.goAndClose(section.url, section.state, section.anchor, $event, section)\"><i ng-if=\"section.icon\" class=\"{{section.icon}}\"></i><md-icon ng-if=\"section.iconMi\" md-font-set=\"material-icons\">{{section.iconMi}}</md-icon><span>{{section | menuHuman }}</span></md-button>");
$templateCache.put("core/page/menu/menuToggle.tpl.html","<md-button class=\"md-button-toggle\" ng-click=\"toggle()\" aria-controls=\"app-menu-{{section.name | nospace}}\" flex=\"\" layout=\"row\" aria-expanded=\"{{isOpen()}}\"><i ng-if=\"section.icon\" class=\"{{section.icon}}\"></i> <span class=\"title\">{{section.name}}</span> <span aria-hidden=\"true\" class=\"md-toggle-icon\" ng-class=\"{\'toggled\' : isOpen()}\"></span></md-button><ul ng-show=\"isOpen()\" id=\"app-menu-{{section.name | nospace}}\" class=\"menu-toggle-list\"><li ng-repeat=\"page in section.pages\"><div layout=\"row\"><menu-link section=\"page\" flex=\"\"></menu-link><md-button flex=\"25\" ng-click=\"cart.add(page._)\" aria-label=\"adicione {{page.name}} ao carrinho\" title=\"adicione {{page.name}} ao carrinho\" ng-if=\"section.product\"><i class=\"fa fa-cart-plus\"></i></md-button></div></li></ul>");
$templateCache.put("core/page/menu/sidenav.tpl.html","<div layout=\"column\"><menu-facepile ng-if=\"app.user.current(\'company\').facebook && (app.state.current.name!=\'app.home\' && app.state.current.name!=\'app.account\') && app.enviroment !== \'development\' && !app.iframe\" hide-sm=\"\" width=\"304\" url=\"https://www.facebook.com/{{app.user.current(\'company\').facebook}}\" facepile=\"true\" hide-cover=\"false\" ng-hide=\"app.state.current.name===\'app.pages\'\"></menu-facepile><menu-avatar first-name=\"app.user.profile.firstName\" last-name=\"app.user.profile.lastName\" gender=\"app.user.profile.gender\" facebook=\"app.user.facebook\"></menu-avatar><div flex=\"\"><ul class=\"app-menu\"><li ng-repeat=\"section in app.menu.sections\" class=\"parent-list-item\" ng-class=\"{\'parentActive\' : app.menu.isSectionSelected(section)}\"><h2 class=\"menu-heading\" ng-if=\"section.type === \'heading\'\" id=\"heading_{{ section.name | nospace }}\" layout=\"row\"><i ng-if=\"section.icon\" class=\"{{section.icon}}\"></i><md-icon ng-if=\"section.iconMi\" md-font-set=\"material-icons\">{{section.icon}}</md-icon><my-svg-icon ng-if=\"section.iconSvg\" class=\"ic_24px\" icon=\"{{section.iconSvg}}\"></my-svg-icon><span>{{section.name}}</span></h2><menu-link section=\"section\" ng-if=\"section.type === \'link\'\"></menu-link><menu-toggle section=\"section\" ng-if=\"section.type === \'toggle\'\"></menu-toggle><ul ng-if=\"section.children\" class=\"menu-nested-list\"><li ng-repeat=\"child in section.children\" ng-class=\"{\'childActive\' : app.menu.isChildSectionSelected(child)}\"><menu-toggle section=\"child\"></menu-toggle></li></ul></li><li><a class=\"md-button md-default-theme\" ng-click=\"app.logout()\"><i class=\"fa fa-power-off\"></i> <span class=\"title\">Sair</span></a></li></ul></div><div layout=\"column\" layout-align=\"center center\" class=\"page-footer text-center\"><md-content flex=\"\" class=\"main-wrapper\"><div class=\"copyright\"><strong>{{ app.setting.copyright }} © {{ app.year }}</strong></div><div class=\"terms\"><a ui-sref=\"app.pages({slug:\'privacy\'})\">Política de Privacidade</a> - <a ui-sref=\"app.pages({slug:\'terms\'})\">Termos de Serviço</a></div></md-content></div></div>");
$templateCache.put("core/page/toolbar/toolbar.tpl.html","<div class=\"md-toolbar-tools\" layout=\"row\" layout-align=\"space-between center\"><div hide=\"\" show-sm=\"\" show-md=\"\" layout=\"row\"><a ng-click=\"app.menu.open()\" ng-if=\"app.isAuthed()\" aria-label=\"menu\"><md-icon md-svg-src=\"assets/images/icons/ic_menu_24px.svg\"></md-icon></a><toolbar-title hide-sm=\"\" hide-md=\"\"></toolbar-title></div><toolbar-title hide=\"\" show-gt-md=\"\"></toolbar-title><div layout=\"row\" ng-if=\"app.state.current.name != \'app.home\'\"><ul class=\"top-menu\"><li></li></ul><toolbar-menu ng-if=\"app.isAuthed()\"></toolbar-menu><a ui-sref=\"app.home\"><img hide=\"\" show-sm=\"\" show-md=\"\" class=\"logo-header\" ng-src=\"{{app.logoWhite}}\"></a></div></div>");
$templateCache.put("core/page/toolbar/menu/toolbarMenu.tpl.html","<ul class=\"top-menu\"><li ng-repeat=\"item in menu\"><a id=\"{{item.id}}\" title=\"{{item.name}}\"><i class=\"{{item.icon}}\"></i></a></li></ul>");
$templateCache.put("core/page/toolbar/title/toolbarTitle.tpl.html","<div class=\"logo-company\" layout=\"row\" layout-align=\"space-between center\"><a href=\"/\"><img class=\"logo-header\" ng-src=\"{{app.logoWhite}}\"></a></div>");
$templateCache.put("core/utils/directives/ceper/ceper.tpl.html","<md-input-container class=\"ceper\" flex=\"\"><label><div clayout=\"row\"><label>Cep</label><md-progress-circular class=\"load\" md-mode=\"indeterminate\" md-diameter=\"18\" ng-show=\"vm.busy\"></md-progress-circular></div></label> <input type=\"text\" ng-minlength=\"\'8\'\" ng-maxlength=\"\'8\'\" ng-model=\"ngModel\" ng-change=\"vm.get()\" required=\"\"></md-input-container>");
$templateCache.put("core/utils/directives/contactForm/contactForm.tpl.html","<form name=\"handleForm\" class=\"contact-form\"><div layout=\"row\" layout-sm=\"column\"><md-input-container flex=\"\"><label>Nome</label> <input ng-model=\"ngModel.name\" required=\"\"></md-input-container><md-input-container flex=\"\"><label>Email</label> <input type=\"email\" ng-model=\"ngModel.email\" required=\"\"></md-input-container><md-input-container flex=\"\"><label>Celular</label> <input ng-model=\"ngModel.mobile\" ui-br-phone-number=\"\" required=\"\"></md-input-container><md-input-container flex=\"\"><label>Telefone</label> <input ng-model=\"ngModel.phone\" ui-br-phone-number=\"\"></md-input-container></div><md-button class=\"md-fab md-primary md-hue-2 save\" aria-label=\"Salvar\" ng-if=\"endpointUrl\" ng-click=\"vm.save()\" ng-disabled=\"vm.busy||handleForm.$invalid||!handleForm.$dirty||vm.pristine()\"><md-tooltip>Salvar</md-tooltip><i class=\"fa fa-thumbs-up\"></i></md-button></form>");
$templateCache.put("core/utils/directives/imageCutter/imageCutter.tpl.html","<div class=\"image-cutter-wrapper\"><ng-transclude ng-click=\"modal($event)\" ng-if=\"cutOnModal===\'true\'\"></ng-transclude><image-cutter-area ng-if=\"cutOnModal != \'true\'\" endpoint-url=\"{{endpointUrl}}\" endpoint-params=\"endpointParams\" endpoint-success=\"endpointSuccess\" endpoint-fail=\"endpointFail\" cut-on-modal=\"{{cutOnModal}}\" cut-width=\"{{cutWidth}}\" cut-height=\"{{cutHeight}}\" cut-shape=\"{{cutShape}}\" cut-label=\"{{cutLabel}}\" cut-result=\"cutResult\" cut-step=\"cutStep\"></image-cutter-area></div>");
$templateCache.put("core/utils/directives/imageCutter/modal.tpl.html","<md-dialog class=\"image-cutter-wrapper\" aria-label=\"{{cutOnModalTitle}}\"><md-toolbar><div class=\"md-toolbar-tools\"><h5>{{cutOnModalTitle}}</h5><span flex=\"\"></span><md-button class=\"close md-icon-button\" ng-click=\"hide()\"><i class=\"material-icons\">&#xE14C;</i></md-button></div></md-toolbar><md-dialog-content><p ng-if=\"cutText\">{{cutText}}</p><image-cutter-area endpoint-url=\"{{endpointUrl}}\" endpoint-params=\"endpointParams\" endpoint-success=\"endpointSuccess\" endpoint-fail=\"endpointFail\" cut-on-modal=\"{{cutOnModal}}\" cut-width=\"{{cutWidth}}\" cut-height=\"{{cutHeight}}\" cut-shape=\"{{cutShape}}\" cut-label=\"{{cutLabel}}\" cut-result=\"cutResult\" cut-step=\"cutStep\"></image-cutter-area></md-dialog-content></md-dialog>");
$templateCache.put("core/utils/directives/toolbarAvatar/toolbarAvatar.tpl.html","<div class=\"toolbar-avatar\"><md-menu><md-button aria-label=\"Open phone interactions menu\" ng-click=\"$mdOpenMenu()\" class=\"logged-in-menu-button\" ng-class=\"{\'md-icon-button\': app.mdMedia(\'sm\')}\"><div layout=\"row\" layout-align=\"end center\" class=\"toolbar-login-info\"><div layout=\"column\" layout-align=\"center\" class=\"toolbar-login-content\" show-gt-sm=\"\" hide-sm=\"\"><span class=\"md-title\">{{firstName}}</span> <span class=\"md-caption\">{{email}}</span></div><div layout=\"row\" layout-align=\"center center\"><menu-avatar facebook=\"facebook\" md-menu-origin=\"\"></menu-avatar></div></div></md-button><md-menu-content width=\"4\"><md-menu-item ng-repeat=\"item in menu\"><md-button ng-href=\"{{item.href}}\"><md-icon md-font-icon=\"fa {{item.icon}}\" md-menu-align-target=\"\"></md-icon>{{item.title}}</md-button></md-menu-item><md-menu-divider></md-menu-divider><md-menu-item><md-button ng-click=\"vm.logout()\"><md-icon md-font-icon=\"fa fa-power-off\" md-menu-align-target=\"\"></md-icon>Sair</md-button></md-menu-item></md-menu-content></md-menu></div>");
$templateCache.put("core/utils/directives/imageCutter/area/imageCutterArea.tpl.html","<div class=\"image-cutter\"><image-crop data-width=\"{{cutWidth}}\" data-height=\"{{cutHeight}}\" data-shape=\"{{cutShape}}\" data-step=\"cutStep\" data-result=\"cutResult\"></image-crop><div hide=\"\"><md-button class=\"refresh md-raised\" ng-click=\"reboot()\" aria-label=\"Recomeçar\"><i class=\"fa fa-refresh\"></i><md-tooltip>Recomeçar</md-tooltip></md-button><div class=\"progress\" ng-show=\"busy\"><md-progress-circular class=\"md-hue-2\" md-mode=\"indeterminate\"></md-progress-circular></div></div></div>");}]);