# app-kit
esqueleto para aplicações

## install
- 1: `git clone https://github.com/esgrupo/app-kit-seed my_app`
- 2: `cd my_app`
- 3: `npm install && bower install`
- 4: find & replace `my_app` instead of `app.seed`
- 5: `gulp serve`

